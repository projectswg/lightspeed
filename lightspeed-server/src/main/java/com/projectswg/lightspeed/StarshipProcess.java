/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed;

import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;

import java.io.*;
import java.nio.charset.StandardCharsets;
import java.util.concurrent.TimeUnit;

public class StarshipProcess {
	
	private final Object processMutex;
	private final File dir;
	private final String [] commands;
	private Process process;
	private int exitValue;
	
	private StarshipProcess(File dir, String [] commands) {
		if (dir == null)
			throw new NullPointerException("Directory cannot be null!");
		if (commands == null)
			throw new NullPointerException("");
		this.processMutex = new Object();
		this.dir = dir;
		this.commands = new String[commands.length+1];
		System.arraycopy(commands, 0, this.commands, 0, commands.length);
		this.commands[commands.length] = "";
		this.process = null;
		this.exitValue = Integer.MAX_VALUE;
	}
	
	private ProcessBuilder createBuilder(int adminServerPort) {
		commands[commands.length-1] = "-adminServerPort=" + adminServerPort;
		return new ProcessBuilder(commands);
	}
	
	/**
	 * Creates and starts the process with the specified jar, directory, and arguments. If the
	 * process is already started, or started successfully then this method returns TRUE. Otherwise
	 * returns false
	 * 
	 * @return TRUE if a process has started or was already started, FALSE otherwise
	 */
	public boolean start(int adminServerPort) {
		synchronized (processMutex) {
			if (process != null)
				return true;
			ProcessBuilder builder = createBuilder(adminServerPort);
			builder.redirectErrorStream(true).directory(dir);
			Process p = null;
			try {
				p = builder.start();
			} catch (IOException e) {
				p = null;
				Log.e(e);
			}
			process = p;
			return process != null;
		}
	}
	
	/**
	 * Stops the process and waits for complete termination. After 5 seconds or being interrupted,
	 * it returns anyways with Integer.MAX_VALUE. If it terminates normally, this method returns
	 * it's exit value
	 * 
	 * @return the process' exit value or Integer.MAX_VALUE if timed out/interrupted
	 */
	public int stop() {
		synchronized (processMutex) {
			if (process == null)
				return exitValue;
			process.destroy();
			try {
				if (process.waitFor(5, TimeUnit.SECONDS))
					exitValue = process.exitValue();
				else
					exitValue = Integer.MAX_VALUE;
			} catch (InterruptedException e) {
				exitValue = Integer.MAX_VALUE;
			}
			process = null;
		}
		return exitValue;
	}
	
	public void gobbleOutput(ServerDeploymentData deployment) {
		try (InputStream is = process.getInputStream()) {
			byte [] buffer = new byte[256];
			int bytes;
			SharedDeploymentData sharedDeployment = deployment.getShared();
			while ((bytes = is.read(buffer)) >= 0) {
				sharedDeployment.appendOutput(new String(buffer, 0, bytes, StandardCharsets.UTF_8)); // gobble.
			}
		} catch (IOException e) {
			if (Assert.debug() && (e.getMessage() == null || !e.getMessage().toLowerCase().contains("closed")))
				Log.e(e);
		}
		try (BufferedReader gobbler = new BufferedReader(new InputStreamReader(process.getInputStream()))) {
			String line;
			SharedDeploymentData sharedDeployment = deployment.getShared();
			while ((line = gobbler.readLine()) != null) {
				sharedDeployment.appendOutput(line); // gobble.
			}
		} catch (IOException e) {
			if (Assert.debug() && (e.getMessage() == null || !e.getMessage().toLowerCase().contains("closed")))
				Log.e(e);
		}
	}
	
	/**
	 * Waits for the process to terminate and returns the exit value. If this thread is interrupted
	 * before the process terminates, Integer.MAX_VALUE is returned
	 * 
	 * @return the exit value or Integer.MAX_VALUE if interrupted
	 */
	public int waitFor() {
		synchronized (processMutex) {
			try {
				if (process == null)
					return exitValue;
				exitValue = process.waitFor();
			} catch (InterruptedException e) {
				exitValue = Integer.MAX_VALUE;
			}
			return exitValue;
		}
	}
	
	public boolean isAlive() {
		synchronized (processMutex) {
			if (process != null)
				return process.isAlive();
			return false;
		}
	}
	
	public static StarshipProcess create(File dir, String [] commands) {
		return new StarshipProcess(dir, commands);
	}
	
}
