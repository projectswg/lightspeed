/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.gui;

import com.projectswg.lightspeed.gui.GUIService.GUIData;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.util.concurrent.atomic.AtomicReference;

public class GUI extends Application {
	
	public static final AtomicReference<GUI> INSTANCE = new AtomicReference<>(null);
	public static final AtomicReference<GUIData> DATA = new AtomicReference<>(null);
	
	private final LightspeedServerPrimaryView primaryView;
	private Stage primaryStage;
	
	public GUI() {
		this.primaryView = new LightspeedServerPrimaryView();
		this.primaryStage = null;
	}
	
	public static GUI getInstance() {
		return INSTANCE.get();
	}
	
	public static void setData(GUIData data) {
		DATA.set(data);
	}
	
	@Override
	public void init() throws Exception {
		super.init();
	}
	
	@Override
	public void start(Stage stage) throws Exception {
		INSTANCE.set(this);
		this.primaryStage = stage;
		stage.setOpacity(0);
		stage.setTitle("Lightspeed Server");
		stage.setScene(new Scene(primaryView));
		stage.show();
		int width = 480;
		int height = 240;
		stage.setMinWidth(width);
		stage.setMinHeight(height);
		stage.setMaxWidth(width);
		stage.setMaxHeight(height);
		stage.setOpacity(1);
		stage.centerOnScreen();
		primaryView.start();
	}
	
	@Override
	public void stop() throws Exception {
		super.stop();
		primaryView.stop();
	}
	
	public void requestStop() {
		Platform.runLater(() -> {
			primaryStage.close();
		});
	}
	
}
