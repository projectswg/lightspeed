/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.gui;

import com.projectswg.common.concurrency.PswgBasicScheduledThread;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;
import com.projectswg.lightspeed.data.server.ServerServerData;
import com.projectswg.lightspeed.gui.GUIService.GUIData;
import javafx.application.Platform;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.scene.text.TextAlignment;

import java.util.HashMap;
import java.util.Locale;
import java.util.Map;

public class LightspeedServerPrimaryView extends VBox {
	
	private final GUIData data;
	private final Map<String, ServerBox> servers;
	private final PswgBasicScheduledThread serverUpdaterThread;
	
	public LightspeedServerPrimaryView() {
		this.data = GUI.DATA.get();
		this.servers = new HashMap<>();
		this.serverUpdaterThread = new PswgBasicScheduledThread("gui-server-updater", () -> updateServers());
		createLargeDisplayText();
		createHeaderBox();
		for (ServerServerData server : data.getBackendData().getServerList()) {
			ServerBox box = new ServerBox(server);
			getChildren().add(box);
			servers.put(server.getName(), box);
		}
	}
	
	public void start() {
		serverUpdaterThread.startWithFixedRate(0, 500);
	}
	
	public void stop() {
		serverUpdaterThread.stop();
	}
	
	private void createLargeDisplayText() {
		Label lightspeed = new Label("Lightspeed");
		lightspeed.setFont(Font.font(null, FontWeight.BOLD, 18));
		lightspeed.setAlignment(Pos.CENTER);
		lightspeed.setMaxWidth(Double.MAX_VALUE);
		lightspeed.setPadding(new Insets(5, 5, 5, 5));
		getChildren().add(lightspeed);
	}
	
	private void createHeaderBox() {
		HBox box = new HBox();
		Label serverLabel = new Label("Server");
		Label buildLabel = new Label("Build");
		Label deploymentLabel = new Label("Deployment");
		Font font = Font.font(null, FontWeight.BOLD, 14);
		box.setMinHeight(30);
		box.setPadding(new Insets(10, 10, 10, 10));
		box.setAlignment(Pos.CENTER_LEFT);
		BorderStroke solid = new BorderStroke(null, BorderStrokeStyle.SOLID, null, null);
		BorderStroke none = new BorderStroke(null, BorderStrokeStyle.NONE, null, null);
		box.setBorder(new Border(solid, none, solid, none));
		serverLabel.setPrefWidth(100);
		buildLabel.setPrefWidth(180);
		deploymentLabel.setPrefWidth(180);
		serverLabel.setFont(font);
		buildLabel.setFont(font);
		deploymentLabel.setFont(font);
		box.getChildren().setAll(serverLabel, buildLabel, deploymentLabel);
		getChildren().add(box);
	}
	
	private void updateServers() {
		for (ServerBox server : servers.values()) {
			server.update();
		}
	}
	
	private static class ServerBox extends HBox {
		
		private final ServerServerData server;
		private final Label serverLabel;
		private final Label buildLabel;
		private final Label deploymentLabel;
		
		public ServerBox(ServerServerData server) {
			this.server = server;
			this.serverLabel = new Label(server.getName());
			this.buildLabel = new Label("");
			this.deploymentLabel = new Label("");
			setMinHeight(50);
			setPadding(new Insets(10, 10, 10, 10));
			setAlignment(Pos.CENTER_LEFT);
			BorderStroke solid = new BorderStroke(null, BorderStrokeStyle.SOLID, null, null);
			BorderStroke none = new BorderStroke(null, BorderStrokeStyle.NONE, null, null);
			setBorder(new Border(solid, none, solid, none));
			serverLabel.setPrefWidth(100);
			buildLabel.setPrefWidth(180);
			deploymentLabel.setPrefWidth(180);
			buildLabel.setTextAlignment(TextAlignment.CENTER);
			deploymentLabel.setTextAlignment(TextAlignment.CENTER);
			serverLabel.setFont(Font.font(null, FontWeight.BOLD, 14));
			getChildren().setAll(serverLabel, buildLabel, deploymentLabel);
			update();
		}
		
		public void update() {
			Platform.runLater(() -> {
				updateBuild();
				updateDeployment();
				requestLayout();
			});
		}
		
		private void updateBuild() {
			ServerBuildData build = server.getLastBuild();
			if (build == null) {
				buildLabel.setText("Not Built");
			} else {
				switch (build.getBuildState()) {
					case COMPILING:		buildLabel.setText("Compiling...");		break;
					case TESTING:		buildLabel.setText("Testing...");		break;
					case INSTALLING:	buildLabel.setText("Installing...");	break;
					case COMPILE_FAILED:buildLabel.setText("Compile Failed");	break;
					case TEST_FAILED:	buildLabel.setText("Tests Failed");		break;
					case INSTALL_FAILED:buildLabel.setText("Install Failed");	break;
					case CANCELLED:		buildLabel.setText("Cancelled");		break;
					case SUCCESS:		buildLabel.setText("Success");			break;
					default:			buildLabel.setText(build.getBuildState().name()); break;
				}
			}
		}
		
		private void updateDeployment() {
			ServerDeploymentData deployment = server.getLastDeployment();
			if (deployment == null) {
				deploymentLabel.setText("Not Deployed");
			} else {
				String status = deployment.getDeploymentState().name().toLowerCase(Locale.US);
				status = Character.toUpperCase(status.charAt(0)) + status.substring(1);
				status = status.replace('_', ' ');
				long end = 0;
				if (deployment.isAlive()) {
					end = TimeUtilities.getTime();
				} else {
					end = deployment.getEndTime();
				}
				long time = end - deployment.getStartTime();
				int hours = (int) (time / (3600E3));
				int minutes = (int) ((time % 3600E3) / 60E3);
				int seconds = (int) (time % 60E3) / 1000;
				deploymentLabel.setText(String.format("%s\nTime: %02d:%02d:%02d", deployment.getDeploymentState(), hours, minutes, seconds));
			}
		}
		
	}
	
}
