/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.sequenced;

import com.projectswg.common.debug.Log;

import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;
import java.util.concurrent.atomic.AtomicReference;
import java.util.concurrent.locks.LockSupport;


public abstract class SequencedStarter {
	
	private final Thread shutdownThread;
	private final AtomicLong shutdownWait;
	private final AtomicLong additionalWait;
	private final AtomicReference<SequencedStarter> replacement;
	
	public SequencedStarter() {
		this.shutdownThread = new Thread(() -> shutdownWait(), "sequenced-shutdown");
		this.shutdownWait = new AtomicLong(0);
		this.additionalWait = new AtomicLong(0);
		this.replacement = new AtomicReference<>(null);
	}
	
	public final void setAdditionalWait(long time, TimeUnit unit) {
		additionalWait.set(unit.toNanos(time));
	}
	
	public final void setReplacement(SequencedStarter replacement) {
		this.replacement.set(replacement);
	}
	
	public final void start() {
		onStart();
	}
	
	public final void stop(long time, TimeUnit unit) {
		shutdownWait.set(unit.toNanos(time));
		shutdownThread.start();
	}
	
	public final void stop() {
		try {
			onStop();
		} catch (Throwable t) {
			Log.e(t);
		}
		if (replacement.get() != null)
			replacement.get().start();
	}
	
	protected abstract void onStart();
	protected abstract void onStop();
	
	protected void interruptSleep() {
		shutdownThread.interrupt();
	}
	
	private void shutdownWait() {
		LockSupport.parkNanos(shutdownWait.get()); // Whatever the desired wait time has been set to
		LockSupport.parkNanos(additionalWait.get()); // Any additional time required
		stop();
	}
	
}
