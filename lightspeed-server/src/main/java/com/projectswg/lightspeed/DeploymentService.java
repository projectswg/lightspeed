/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed;

import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedDeploymentData.DeploymentState;
import com.projectswg.common.data.info.Config;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.LightspeedPacketType;
import com.projectswg.common.network.packets.post.PostDeployPacket;
import com.projectswg.common.network.packets.post.PostResult;
import com.projectswg.common.network.packets.post.PostStopDeployPacket;
import com.projectswg.common.network.packets.request.RequestDeploymentDetailedPacket;
import com.projectswg.common.network.packets.request.RequestDeploymentListPacket;
import com.projectswg.common.network.packets.response.ResponseDeploymentDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseDeploymentListPacket;
import com.projectswg.common.network.packets.swg.admin.AdminShutdownServer;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.connection.HolocoreSocket;
import com.projectswg.connection.ServerConnectionChangedReason;
import com.projectswg.lightspeed.control.LightspeedService;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;
import com.projectswg.lightspeed.data.info.ConfigFile;
import com.projectswg.lightspeed.data.server.ServerServerData;
import com.projectswg.lightspeed.intents.LaunchStarshipIntent;
import com.projectswg.lightspeed.intents.RegisterHttpListenerIntent;
import com.projectswg.lightspeed.sequenced.SequencedStarter;
import me.joshlarson.json.JSONObject;

import java.io.File;
import java.net.InetAddress;
import java.util.*;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.atomic.AtomicLong;

public class DeploymentService extends LightspeedService {
	
	private final AtomicLong deploymentId;
	private final Map<String, Deployment> deployments;
	private final Set<ServerDeploymentData> deploymentHistory;
	
	public DeploymentService() {
		deploymentId = new AtomicLong(0);
		deployments = new HashMap<>();
		deploymentHistory = new HashSet<>();
		
		registerForIntent(LaunchStarshipIntent.class, this::handleLaunchStarshipIntent);
	}
	
	@Override
	public boolean initialize() {
		for (ServerServerData server : getBackendData().getServerList()) {
			deploymentHistory.addAll(server.getDeployments());
		}
		for (ServerDeploymentData deployment : deploymentHistory) {
			if (deployment.getId() > deploymentId.get()) {
				deploymentId.set(deployment.getId());
			}
		}
		new RegisterHttpListenerIntent(LightspeedPacketType.POST_DEPLOY, (type, packet) -> processPostDeploy(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.POST_STOP_DEPLOY, (type, packet) -> processStopDeployment(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_DEPLOYMENT_LIST, (type, packet) -> processDeploymentList(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_DEPLOYMENT_DETAILED, (type, packet) -> processDeploymentDetailed(packet)).broadcast();
		return super.initialize();
	}
	
	@Override
	public boolean terminate() {
		synchronized (deployments) {
			for (Deployment deployment : new ArrayList<>(deployments.values())) {
				deployment.shutdown();
			}
		}
		return super.terminate();
	}
	
	private void handleLaunchStarshipIntent(LaunchStarshipIntent lsi) {
		launch(lsi.getBuild(), lsi.isRedeploy());
	}
	
	private File getJdk() {
		Config config = getConfig(ConfigFile.LIGHTSPEED);
		String path = config.getString("jdk", System.getProperty("java.home"));
		if (path.isEmpty()) {
			Log.e("Invalid path for jdk! Empty");
			throw new IllegalStateException("JDK path is invalid!");
		}
		File jdk = new File(path);
		if (!jdk.isDirectory()) {
			Log.e("Invalid path for jdk! Not a directory");
			throw new IllegalStateException("JDK path is invalid!");
		}
		return jdk;
	}
	
	private JSONObject processPostDeploy(Packet packet) {
		PostDeployPacket p = (PostDeployPacket) packet;
		ServerServerData server = getBackendData().getServer(p.getServerId());
		if (server == null)
			return new PostResult(false).getJSON();
		launchStarship(server);
		return new PostResult(true).getJSON();
	}
	
	private JSONObject processStopDeployment(Packet packet) {
		PostStopDeployPacket p = (PostStopDeployPacket) packet;
		synchronized (deployments) {
			Deployment d = deployments.get(p.getServerId());
			if (d != null && d.get().isAlive()) {
				d.shutdown();
				return new PostResult(true).getJSON();
			}
		}
		return new PostResult(false).getJSON();
	}
	
	private JSONObject processDeploymentDetailed(Packet packet) {
		RequestDeploymentDetailedPacket p = (RequestDeploymentDetailedPacket) packet;
		synchronized (deploymentHistory) {
			for (ServerDeploymentData d : deploymentHistory) {
				if (d.getId() == p.getDeploymentId()) {
					return new ResponseDeploymentDetailedPacket(d.getShared()).getJSON();
				}
			}
		}
		return null;
	}
	
	private JSONObject processDeploymentList(Packet packet) {
		RequestDeploymentListPacket p = (RequestDeploymentListPacket) packet;
		List<SharedDeploymentData> deployList = new ArrayList<>();
		synchronized (deploymentHistory) {
			for (ServerDeploymentData d : deploymentHistory) {
				if (d.getServer().getName().equals(p.getServerId())) {
					deployList.add(d.getShared());
				}
			}
		}
		return new ResponseDeploymentListPacket(p.getServerId(), deployList).getJSON();
	}
	
	private void launchStarship(ServerServerData server) {
		ServerBuildData build = server.getLastBuild();
		if (!launchStarshipChecks(build))
			return;
		new LaunchStarshipIntent(build, false).broadcast();
	}
	
	private boolean launchStarshipChecks(ServerBuildData build) {
		if (build == null) {
			Log.e("Unable to launch - no build found!");
			return false;
		}
		if (build.getBuildState() != BuildState.SUCCESS) {
			Log.e("Unable to launch %s/%d - state: %s", build.getServer().getName(), build.getId(), build.getBuildState());
			return false;
		}
		return true;
	}
	
	private void launch(ServerBuildData build, boolean redeploy) {
		Assert.notNull(build.getServer());
		String serverId = build.getServer().getName();
		if (isDeploying(serverId) && !redeploy) {
			Log.e("Unable to deploy %s. Already deploying!", serverId);
			return;
		}
		
		synchronized (deployments) {
			Deployment old = deployments.get(serverId);
			if (old != null && old.get().isAlive() && !redeploy) {
				Log.e("Unable to deploy %s. Already deploying!", serverId);
				return;
			}
			ServerDeploymentData deployment = new ServerDeploymentData(deploymentId.incrementAndGet(), build);
			Log.i("Creating Deployment #%d from build #%d and server %s", deployment.getId(), build.getId(), deployment.getServer().getName());
			Deployment newDeployment = new Deployment(deployment);
			if (old != null) {
				old.setReplacement(newDeployment);
				old.shutdown();
			} else {
				newDeployment.start();
			}
		}
	}
	
	private boolean isDeploying(String serverId) {
		synchronized (deployments) {
			Deployment deployment = deployments.get(serverId);
			return deployment != null && deployment.get().isAlive();
		}
	}
	
	private class Deployment extends SequencedStarter {
		
		private final ServerDeploymentData deployment;
		private HolocoreSocket socket;
		
		public Deployment(ServerDeploymentData deployment) {
			this.deployment = deployment;
			this.socket = null;
		}
		
		public ServerDeploymentData get() {
			return deployment;
		}
		
		public void shutdown() {
			Log.i("Shutting down old deployment of %s", deployment.getServer().getName());
			socket = new HolocoreSocket(InetAddress.getLoopbackAddress(), deployment.getAdminServerPort());
			setAdditionalWait(10, TimeUnit.SECONDS);
			deployment.setDeploymentState(DeploymentState.SHUTTING_DOWN);
			if (!socket.connect(5000)) {
				forceShutdown("CONNECT");
			} else if (!socket.send(new AdminShutdownServer(10).encode().array())) {
				forceShutdown("SHUTDOWN");
			} else {
				stop(10, TimeUnit.SECONDS);
			}
		}
		
		private void forceShutdown(String error) {
			Log.w("Unable to shutdown kindly [%s] - forcing shutdown.", error);
			stop();
		}
		
		@Override
		protected void onStart() {
			synchronized (deploymentHistory) {
				deploymentHistory.add(deployment);
			}
			synchronized (deployments) {
				deployments.put(deployment.getServer().getName(), this);
			}
			Log.i("Deploying %s", deployment.getServer().getName());
			launchProcess(deployment);
		}
		
		@Override
		protected void onStop() {
			synchronized (deployments) {
				String name = deployment.getServer().getName();
				if (deployments.get(name) == this)
					deployments.remove(name);
			}
			socket.disconnect(ServerConnectionChangedReason.NONE);
			socket.terminate();
			StarshipProcess process = deployment.getProcess();
			if (process != null)
				process.stop();
		}
		
		private void launchProcess(ServerDeploymentData deployment) {
			StarshipProcess process = prepareProcess(deployment);
			deployment.setProcess(process);
			if (!process.start(deployment.getAdminServerPort())) {
				Log.e("Failed to deploy %s", deployment.getServer().getName());
				return;
			}
			onDeploymentStarted(deployment);
			new Thread(() -> {
				process.gobbleOutput(deployment);
				process.waitFor();
				process.stop();
				onDeploymentEnded(deployment);
			}, "deployment-"+deployment.getServer().getName()+"-supervisor").start();
		}
		
		private StarshipProcess prepareProcess(ServerDeploymentData deployment) {
			File java = new File(getJdk(), File.separatorChar+"bin"+File.separatorChar+"java");
			File jar = new File("data"+File.separatorChar+"releases"+File.separatorChar+deployment.getServer().getName()+".jar");
			File dir = new File(deployment.getServer().getDirectory());
			String [] jvmArguments = deployment.getServer().getJvmArguments().split(" ");
			String [] command = createStringArray(java.getAbsolutePath(), jvmArguments, "-jar", jar);
			Log.d("Command: %s", Arrays.toString(command));
			return StarshipProcess.create(dir, command);
		}
		
		private String [] createStringArray(Object ... objects) {
			int size = 0;
			for (Object o : objects) {
				if (o instanceof String) {
					if (!((String) o).isEmpty())
						size++;
				} else if (o instanceof String[]) {
					for (String str : (String[]) o) {
						if (!str.isEmpty())
							size++;
					}
				} else if (o instanceof File) {
					size++;
				} else {
					throw new IllegalArgumentException("Invalid argument: " + o);
				}
			}
			String [] ret = new String[size];
			int index = 0;
			for (Object o : objects) {
				if (o instanceof String) {
					if (!((String) o).isEmpty())
						ret[index++] = (String) o;
				} else if (o instanceof File) {
					ret[index++] = ((File) o).getAbsolutePath();
				} else if (o instanceof String[]) {
					for (String str : (String[]) o) {
						if (!str.isEmpty())
							ret[index++] = str;
					}
				}
			}
			return ret;
		}
		
		private void onDeploymentStarted(ServerDeploymentData deployment) {
			deployment.setStartTime(TimeUtilities.getTime());
			deployment.setDeploymentState(DeploymentState.RUNNING);
		}
		
		private void onDeploymentEnded(ServerDeploymentData deployment) {
			deployment.setEndTime(TimeUtilities.getTime());
			deployment.setDeploymentState(DeploymentState.SHUTDOWN);
			getBackendData().insertDeployment(deployment);
			Log.i("%s has finished deploying after %.2f minutes", deployment.getServer().getName(), (deployment.getEndTime()-deployment.getStartTime())/60E3);
			interruptSleep();
		}
		
	}
	
}
