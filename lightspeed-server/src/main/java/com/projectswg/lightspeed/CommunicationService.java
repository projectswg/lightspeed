/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed;

import com.projectswg.common.concurrency.PswgBasicScheduledThread;
import com.projectswg.common.concurrency.PswgThreadPool;
import com.projectswg.common.data.BCrypt;
import com.projectswg.common.data.CsvTable;
import com.projectswg.common.data.info.Config;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.LightspeedPacketType;
import com.projectswg.lightspeed.communication.HttpResponder;
import com.projectswg.lightspeed.control.LightspeedService;
import com.projectswg.lightspeed.data.info.ConfigFile;
import com.projectswg.lightspeed.intents.RegisterHttpListenerIntent;
import me.joshlarson.json.JSONObject;
import me.joshlarson.json.websocket.server.JSONWebSocketConnection;
import me.joshlarson.json.websocket.server.JSONWebSocketConnectionHandler;
import me.joshlarson.json.websocket.server.JSONWebSocketServer;
import org.nanohttpd.protocols.http.ClientHandler;
import org.nanohttpd.protocols.http.threading.IAsyncRunner;
import org.nanohttpd.protocols.websockets.CloseCode;

import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLServerSocketFactory;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.nio.charset.StandardCharsets;
import java.security.KeyStore;
import java.util.*;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock;

public class CommunicationService extends LightspeedService {
	
	private final JSONWebSocketServer httpServer;
	private final Map<LightspeedPacketType, HttpResponder> responders;
	private final PswgBasicScheduledThread userTableUpdater;
	private final BoundRunner runner;
	private final CsvTable users;
	private final ReadWriteLock userLock;
	private final AtomicBoolean security;
	
	public CommunicationService() {
		this.httpServer = new JSONWebSocketServer(44444);
		this.responders = new HashMap<>();
		this.userTableUpdater = new PswgBasicScheduledThread("comm-user-table-updater", this::updateUserTable);
		this.runner = new BoundRunner(5);
		this.users = new CsvTable(new File("data/users.csv"));
		this.userLock = new ReentrantReadWriteLock(true);
		this.security = new AtomicBoolean(false);
		httpServer.setAsyncRunner(runner);
		httpServer.setHandler(new WebSocketHandler());
		
		registerForIntent(RegisterHttpListenerIntent.class, this::handleRegisterListener);
	}
	
	@Override
	public boolean initialize() {
		Config config = getConfig(ConfigFile.LIGHTSPEED);
		try {
			security.set(config.getBoolean("SECURITY", false));
			if (security.get()) {
				String pubkey = config.getString("PUBKEY", "");
				if (!pubkey.isEmpty()) {
					String pass = config.getString("PUBKEY-PASS", "");
					File cert = new File("data", pubkey);
					Log.i("CommunicationService: Setting up security...");
					httpServer.makeSecure(createSSLSocketFactory(cert, pass), null);
				} else {
					Log.i("CommunicationService: Disabling security - no public key...");
				}
				userTableUpdater.startWithFixedDelay(0, 60000);
			} else {
				Log.i("CommunicationService: Disabling security...");
			}
			runner.start();
			httpServer.start(config.getInt("HTTP-TIMEOUT", 15000), false);
		} catch (IOException e) {
			Log.a(e);
			return false;
		}
		return super.initialize();
	}
	
	@Override
	public boolean terminate() {
		if (security.get()) {
			userTableUpdater.stop();
		}
		runner.stop();
		httpServer.stop();
		return super.terminate();
	}
	
	public int getPort() {
		return httpServer.getListeningPort();
	}
	
	private void send(JSONWebSocketConnection connection, JSONObject response) {
		try {
			connection.send(response);
		} catch (IOException e) {
			Log.e(e);
		}
	}
	
	private SSLServerSocketFactory createSSLSocketFactory(File keystoreFile, String password) throws IOException {
		try {
			char[] passphrase = password.toCharArray();
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());
			InputStream keystoreStream = new FileInputStream(keystoreFile);
			
			keystore.load(keystoreStream, passphrase);
			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keystore, passphrase);
			TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(keystore);
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);
			return ctx.getServerSocketFactory();
		} catch (Exception e) {
			throw new IOException(e.getMessage());
		}
	}
	
	private void updateUserTable() {
		Lock write = userLock.writeLock();
		try {
			write.lock();
			users.read();
		} catch (IOException e) {
			Log.e(e);
		} finally {
			write.unlock();
		}
	}
	
	private boolean authenticate(String user, String pass, String addr) {
		if (!security.get())
			return true;
		Lock readLock = userLock.readLock();
		try {
			readLock.lock();
			for (int i = 0; i < users.getRows(); i++) {
				if (users.getRecord(i, "user").equals(user)) {
					String recordedPass = users.getRecord(i, "pass");
					String tested = BCrypt.hashpw(BCrypt.hashpw(pass, recordedPass), recordedPass);
					boolean valid = tested.equals(recordedPass);
					if (!valid)
						Log.w("Unsuccessful login. User: %s  Addr: %s", user, addr);
					return valid;
				}
			}
			return false;
		} finally {
			readLock.unlock();
		}
	}
	
	private JSONObject getOK(JSONObject obj) {
		obj.put("returnCode", "OK");
		obj.put("returnCodeDescription", "");
		return obj;
	}
	
	private JSONObject getNotFound(String str) {
		return getErrorObject("NOT_FOUND", str);
	}
	
	private JSONObject getServerError(String str) {
		return getErrorObject("INTERNAL_ERROR", str);
	}
	
	private JSONObject getUnauthorized(String str) {
		return getErrorObject("UNAUTHORIZED", str);
	}
	
	private JSONObject getErrorObject(String name, String description) {
		JSONObject obj = new JSONObject();
		obj.put("returnCode", name);
		obj.put("returnCodeDescription", description);
		return obj;
	}
	
	private void handleRegisterListener(RegisterHttpListenerIntent rhli) {
		Log.i("Registering a responder for %s", rhli.getRequestType());
		Assert.isNull(responders.put(rhli.getRequestType(), rhli.getHttpResponder()), "Cannot register two responders for the same packet type!");
	}
	
	private class WebSocketHandler extends JSONWebSocketConnectionHandler {
		
		@Override
		public void onConnect(JSONWebSocketConnection socket) {
			Session session = new Session();
			session.setAuthenticated(false);
			
			try {
				Log.d("%s connected", socket.getRemoteIpAddress());
				String auth = socket.getHandshakeRequest().getHeaders().get("authorization");
				if (auth != null && auth.startsWith("Basic")) {
					String[] combined = new String(Base64.getDecoder().decode(auth.substring(6)), StandardCharsets.UTF_8).split(":", 2);
					if (combined.length != 2) {
						Log.w("%s failed to authenticate - invalid basic authentication!", socket.getRemoteIpAddress());
						socket.close(CloseCode.AbnormalClosure, "Invalid basic authentication");
						return;
					}
					Log.d("%s authenticating...", socket.getRemoteIpAddress());
					boolean success = authenticate(combined[0], combined[1], socket.getRemoteIpAddress());
					
					if (!success) {
						Log.w("%s failed to authenticate - invalid username/password", socket.getRemoteIpAddress());
						socket.close(CloseCode.AbnormalClosure, "Invalid username/password");
						return;
					}
					
					Log.d("%s authenticated with username %s.", socket.getRemoteIpAddress(), combined[0]);
					session.setAuthenticated(true);
				} else {
					Log.w("%s failed to authenticate - no username/password specified", socket.getRemoteIpAddress());
					socket.close(CloseCode.AbnormalClosure, "No username/password specified");
					return;
				}
				socket.setUserData(session);
			} catch (IOException e) {
				Log.e(e);
			}
		}
		
		@Override
		public void onDisconnect(JSONWebSocketConnection socket) {
			Log.d("%s disconnected", socket.getRemoteIpAddress());
		}
		
		@Override
		public void onMessage(JSONWebSocketConnection socket, JSONObject object) {
			Session session = (Session) socket.getUserData();
			LightspeedPacketType request = LightspeedPacketType.valueOf(object.getString("type"));
			Packet packet = Packet.inflate(object, request);
			if (packet == null) {
				Log.e("CommunicationService: Failed to inflate %s [%s]", request, object.getString("type"));
				return;
			}
			
			if (!session.isAuthenticated()) {
				send(socket, getUnauthorized("Not logged in"));
				return;
			}
			
			HttpResponder responder = responders.get(request);
			if (responder == null) {
				send(socket, getNotFound("No Services Ready to Respond to " + request.name()));
				return;
			}
			try {
				JSONObject obj = responder.request(request, packet);
				if (obj == null) {
					send(socket, getNotFound("Invalid Request Parameters"));
				} else {
					send(socket, getOK(obj));
				}
			} catch (Throwable t) {
				Log.w(t);
				send(socket, getServerError(t.toString() + "\nRequest:\n" + object));
			}
		}
		
		@Override
		public void onPong(JSONWebSocketConnection socket, ByteBuffer data) {
			
		}
		
		@Override
		public void onError(JSONWebSocketConnection socket, Throwable t) {
			Log.e("%s %s: %s", socket.getRemoteIpAddress(), t.getClass().getSimpleName(), t.getMessage());
		}
		
	}
	
	private static class BoundRunner implements IAsyncRunner {
		
		private final PswgThreadPool threadPool;
		private final List<ClientHandler> running;
		
		public BoundRunner(int threads) {
			if (threads <= 0)
				throw new IllegalArgumentException("Thread count must be >= 1!");
			if (threads > 1)
				this.threadPool = new PswgThreadPool(threads, "http-thread-%d");
			else
				this.threadPool = new PswgThreadPool(threads, "http-thread");
			this.running = new ArrayList<>();
		}
		
		public void start() {
			threadPool.start();
		}
		
		public void stop() {
			threadPool.stop(false);
			threadPool.awaitTermination(1000);
		}
		
		@Override
		public void closeAll() {
			// copy of the list for concurrency
			synchronized (running) {
				for (ClientHandler clientHandler : running) {
					clientHandler.close();
				}
			}
		}
		
		@Override
		public void closed(ClientHandler clientHandler) {
			synchronized (running) {
				running.remove(clientHandler);
			}
		}
		
		@Override
		public void exec(ClientHandler clientHandler) {
			threadPool.execute(clientHandler);
			synchronized (running) {
				running.add(clientHandler);
			}
		}
	}
	
	private static class Session {
		
		private final AtomicBoolean authenticated;
		
		public Session() {
			this.authenticated = new AtomicBoolean(false);
		}
		
		public void setAuthenticated(boolean authenticated) {
			this.authenticated.set(authenticated);
		}
		
		public boolean isAuthenticated() {
			return authenticated.get();
		}
		
	}
	
}
