/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.git;

import com.projectswg.common.debug.Log;
import org.eclipse.jgit.api.Git;
import org.eclipse.jgit.api.PullResult;
import org.eclipse.jgit.api.errors.GitAPIException;
import org.eclipse.jgit.lib.Ref;
import org.eclipse.jgit.storage.file.FileRepositoryBuilder;

import java.io.File;
import java.io.IOException;
import java.util.Map;


public class GitInterface {
	
	public GitInterface() {
		
	}
	
	public String getLastCommit(String repository) throws IOException {
		try (Git git = new Git(FileRepositoryBuilder.create(new File(repository, ".git")))) {
			Map<String, Ref> remoteRefs = git.lsRemote().setRemote("origin").setTags(false).setHeads(true).callAsMap();
			Ref qa = remoteRefs.get("refs/heads/quality_assurance");
			if (qa == null)
				return "";
			return qa.getObjectId().getName();
		} catch (GitAPIException e) {
			Log.e("Failed to query last commit from repository [%s]: %s", repository, e.getMessage());
		}
		return "";
	}
	
	public boolean pullLastUpdate(String repository, boolean rebase) throws IOException {
		try (Git git = new Git(FileRepositoryBuilder.create(new File(repository, ".git")))) {
			PullResult res = git.pull()
					.setRemote("origin")
					.setRemoteBranchName("quality_assurance")
					.setRebase(rebase)
					.call();
			git.submoduleUpdate().call();
			return res.isSuccessful();
		} catch (GitAPIException e) {
			Log.e("Failed to pull from repository [%s]: %s", repository, e.getMessage());
		}
		return false;
	}
	
}
