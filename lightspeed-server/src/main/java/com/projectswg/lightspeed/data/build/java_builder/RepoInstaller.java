/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build.java_builder;

import java.io.File;
import java.util.ArrayList;
import java.util.List;


public class RepoInstaller {
	
	private static final String SEP = File.separator;
	
	private final File repo;
	private final File bin;
	private final File src;
	private final File lib;
	private final File test;
	private final File jar;
	
	private final File jdk;
	private final File jdkJava;
	private final File jdkJavac;
	private final File jdkJar;
	
	public RepoInstaller(File repo, File jdk) {
		this.repo = repo;
		this.bin = new File(repo, "bin");
		this.src = new File(repo, "src");
		this.lib = new File(repo, "lib");
		this.test= new File(repo, "test");
		this.jar = new File(bin, "dist" + SEP + "ProjectSWG.jar");
		this.jdk = jdk;
		String ext = "";
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			ext = ".exe";
		this.jdkJava = new File(jdk, "bin" + SEP + "java" + ext);
		this.jdkJavac = new File(jdk, "bin" + SEP + "javac" + ext);
		this.jdkJar = new File(jdk, "bin" + SEP + "jar" + ext);
	}
	
	public String getExtension() {
		if (System.getProperty("os.name").toLowerCase().indexOf("win") >= 0)
			return ".exe";
		return "";
	}
	
	public File getRepo() {
		return repo;
	}
	
	public File getBin() {
		return bin;
	}
	
	public File getSrc() {
		return src;
	}
	
	public File getLib() {
		return lib;
	}
	
	public File getTest() {
		return test;
	}
	
	public File getJar() {
		return jar;
	}
	
	public File getJdk() {
		return jdk;
	}
	
	public File getJdkJava() {
		return jdkJava;
	}
	
	public File getJdkJavac() {
		return jdkJavac;
	}
	
	public File getJdkJar() {
		return jdkJar;
	}

	public int execute(String [] command) {
		return execute(command, false);
	}
	
	public int execute(String [] command, StringBuilder output) {
		String opDirectory = repo.getAbsolutePath() + File.separatorChar;
		for (int i = 0; i < command.length; i++) {
			command[i] = command[i].replace(opDirectory, "");
		}
		return CustomJavaCommon.execute(command, repo, output, false);
	}
	
	public int execute(String [] command, boolean output) {
		String opDirectory = repo.getAbsolutePath() + File.separatorChar;
		for (int i = 0; i < command.length; i++) {
			command[i] = command[i].replace(opDirectory, "");
		}
		return CustomJavaCommon.execute(command, repo, null, output);
	}
	
	public String getFilesRelative(File dir, String extension, char delim) {
		return getFiles(dir, extension, delim).replace(repo.getAbsolutePath()+SEP, "");
	}
	
	public String getFiles(File dir, String extension, char delim) {
		List<File> files = getFileList(dir, extension);
		StringBuilder str = new StringBuilder("");
		for (File file : files) {
			str.append(file.getAbsolutePath());
			str.append(delim);
		}
		if (str.length() == 0)
			return "";
		return str.substring(0, str.length()-1);
	}
	
	public List<File> getFileList(File dir, String extension) {
		List<File> files = new ArrayList<>();
		for (String subStr : dir.list()) {
			File sub = new File(dir, subStr);
			if (sub.isDirectory()) {
				files.addAll(getFileList(sub, extension));
			} else if (sub.getName().endsWith(extension)) {
				files.add(sub);
			}
		}
		return files;
	}
	
}
