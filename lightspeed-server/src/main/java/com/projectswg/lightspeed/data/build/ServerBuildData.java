/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.TestDetails;
import com.projectswg.lightspeed.data.server.ServerServerData;

import java.io.File;

public class ServerBuildData {
	
	private final SharedBuildData shared;
	private final ServerServerData server;
	private File jar;
	
	public ServerBuildData(long id, ServerServerData server) {
		this.shared = new SharedBuildData(id, server.getShared());
		this.server = server;
		this.jar = null;
		server.addBuild(this);
	}
	
	public SharedBuildData getShared() {
		return shared;
	}
	
	public ServerServerData getServer() {
		return server;
	}
	
	public long getId() {
		return shared.getId();
	}
	
	public long getTime() {
		return shared.getTime();
	}
	
	public String getBuildString() {
		return shared.getBuildString();
	}
	
	public String getTestString() {
		return shared.getTestString();
	}
	
	public double getCompileTime() {
		return shared.getCompileTime();
	}
	
	public BuildState getBuildState() {
		return shared.getBuildState();
	}
	
	public TestDetails getTestDetails() {
		return shared.getTestDetails();
	}
	
	public File getJar() {
		return jar;
	}
	
	public void setTime(long time) {
		shared.setTime(time);
	}
	
	public void setBuildString(String buildString) {
		shared.setBuildString(buildString);
	}
	
	public void setTestString(String testString) {
		shared.setTestString(testString);
	}
	
	public void setCompileTime(double compileTime) {
		shared.setCompileTime(compileTime);
	}
	
	public void setBuildState(BuildState buildState) {
		shared.setBuildState(buildState);
	}
	
	public void setTestDetails(TestDetails testDetails) {
		shared.setTestDetails(testDetails);
	}
	
	public void setJar(File jar) {
		this.jar = jar;
	}
	
	public interface InstallationCallback {
		void onCompleted(ServerBuildData build);
		void onCancelled(ServerBuildData build);
	}
	
}
