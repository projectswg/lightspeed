/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.sql;

import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.data.info.RelationalDatabase;
import com.projectswg.common.data.info.RelationalServerFactory;
import com.projectswg.common.debug.Log;
import com.projectswg.lightspeed.data.sql.SQLBuildData.BuildInformationBuilder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class BuildHistoryTable implements AutoCloseable {
	
	private static final int STANDARD_HISTORY_SIZE = 25;
	private static final String ALL_COLUMNS = "id, time, server_id, build_string, test_string, compile_time, build_state, test_total, test_failures";
	private static final String INSERT_BUILD_HISTORY_SQL = "INSERT INTO builds ("+ALL_COLUMNS+") VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	private static final String GET_BUILD_HISTORY_SQL = "SELECT "+ALL_COLUMNS+" FROM builds WHERE server_id = ? ORDER BY time DESC";
	private static final String GET_BUILD_BY_ID_SQL = "SELECT "+ALL_COLUMNS+" FROM builds WHERE id = ?";
	
	private final RelationalDatabase database;
	private final PreparedStatement insertBuildHistoryStatement;
	private final PreparedStatement getBuildHistoryStatement;
	private final PreparedStatement getBuildByIdStatement;
	
	public BuildHistoryTable() {
		this.database = RelationalServerFactory.getServerDatabase("lightspeed.db");
		this.insertBuildHistoryStatement = database.prepareStatement(INSERT_BUILD_HISTORY_SQL);
		this.getBuildHistoryStatement = database.prepareStatement(GET_BUILD_HISTORY_SQL);
		this.getBuildByIdStatement = database.prepareStatement(GET_BUILD_BY_ID_SQL);
	}
	
	public BuildHistoryTable(RelationalDatabase db) {
		this.database = null;
		this.insertBuildHistoryStatement = db.prepareStatement(INSERT_BUILD_HISTORY_SQL);
		this.getBuildHistoryStatement = db.prepareStatement(GET_BUILD_HISTORY_SQL);
		this.getBuildByIdStatement = db.prepareStatement(GET_BUILD_BY_ID_SQL);
	}
	
	@Override
	public void close() {
		if (database != null)
			database.close();
		safeClose(insertBuildHistoryStatement);
		safeClose(getBuildHistoryStatement);
		safeClose(getBuildByIdStatement);
	}
	
	public List<SQLBuildData> getBuildHistory(String serverId) {
		List<SQLBuildData> history = new ArrayList<>(STANDARD_HISTORY_SIZE);
		try {
			synchronized (getBuildHistoryStatement) {
				getBuildHistoryStatement.setString(1, serverId);
				ResultSet set = getBuildHistoryStatement.executeQuery();
				while (set.next()) {
					history.add(createBuildFromSet(set));
				}
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return history;
	}
	
	public SQLBuildData getBuildById(long buildId) {
		try {
			synchronized (getBuildByIdStatement) {
				getBuildByIdStatement.setLong(1, buildId);
				ResultSet set = getBuildByIdStatement.executeQuery();
				if (set.next())
					return createBuildFromSet(set);
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return null;
	}
	
	private SQLBuildData createBuildFromSet(ResultSet set) throws SQLException {
		BuildInformationBuilder builder = new BuildInformationBuilder();
		builder.setId(set.getLong("id"));
		builder.setCompletedTime(set.getLong("time"));
		builder.setServerId(set.getString("server_id"));
		builder.setBuildString(set.getString("build_string"));
		builder.setTestString(set.getString("test_string"));
		builder.setCompileTime(set.getDouble("compile_time"));
		builder.setBuildState(BuildState.getState(set.getString("build_state")));
		builder.setTestDetails(new TestDetails(set.getInt("test_total"), set.getInt("test_failures")));
		return builder.build();
	}
	
	public void insertBuildHistory(SQLBuildData build) {
		try {
			synchronized (insertBuildHistoryStatement) {
				insertBuildHistoryStatement.setLong(1, build.getId());
				insertBuildHistoryStatement.setLong(2, build.getCompletedTime());
				insertBuildHistoryStatement.setString(3, build.getServerId());
				insertBuildHistoryStatement.setString(4, build.getBuildString());
				insertBuildHistoryStatement.setString(5, build.getTestString());
				insertBuildHistoryStatement.setDouble(6, build.getCompileTime());
				insertBuildHistoryStatement.setString(7, build.getBuildState().name());
				TestDetails test = build.getTestDetails();
				if (test != null) {
					insertBuildHistoryStatement.setInt(8, test.getTotal());
					insertBuildHistoryStatement.setInt(9, test.getFailures());
				} else {
					insertBuildHistoryStatement.setInt(8, 0);
					insertBuildHistoryStatement.setInt(9, 0);
				}
				insertBuildHistoryStatement.executeUpdate();
			}
		} catch (SQLException e) {
			Log.e(e);
		}
	}
	
	private void safeClose(PreparedStatement statement) {
		try {
			statement.close();
		} catch (SQLException e) {
			// Ignored
		}
	}
	
}
