/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.sql;

import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import me.joshlarson.json.JSONObject;

public class SQLBuildData {
	
	private long id;
	private String serverId;
	private String buildString;
	private String testString;
	private long completedTime;
	private double compileTime;
	private BuildState buildState;
	private TestDetails testDetails;
	
	public SQLBuildData(ServerBuildData build) {
		this.id = build.getId();
		this.serverId = build.getServer().getName();
		this.buildString = build.getBuildString();
		this.testString = build.getTestString();
		this.completedTime = build.getTime();
		this.compileTime = build.getCompileTime();
		this.buildState = build.getBuildState();
		this.testDetails = build.getTestDetails();
	}
	
	public SQLBuildData(JSONObject json) {
		this.id = json.getLong("id");
		this.serverId = json.getString("server_id");
		this.buildString = json.getString("build_string");
		this.testString = json.getString("test_string");
		this.completedTime = TimeUtilities.getTimeFromStringUtc(json.getString("completed_time"));
		this.compileTime = json.getDouble("compile_time");
		this.buildState = BuildState.getState(json.getString("build_state"));
		this.testDetails = new TestDetails(json.getInt("test_total"), json.getInt("test_failures"));
	}
	
	private SQLBuildData() {
		this.id = 0;
		this.serverId = "";
		this.buildString = "";
		this.testString = "";
		this.completedTime = 0;
		this.compileTime = 0;
		this.buildState = BuildState.CREATED;
		this.testDetails = null;
	}
	
	public long getId() {
		return id;
	}
	
	public String getServerId() {
		return serverId;
	}
	
	public String getBuildString() {
		return buildString;
	}
	
	public String getTestString() {
		return testString;
	}
	
	public long getCompletedTime() {
		return completedTime;
	}
	
	public double getCompileTime() {
		return compileTime;
	}
	
	public BuildState getBuildState() {
		return buildState;
	}
	
	public TestDetails getTestDetails() {
		return testDetails;
	}
	
	public String toString() {
		return String.format("BuildInformation[id=%d, server=%s, compile_time=%.3fms, state=%s, test=%s]",
				id, serverId, compileTime, buildState, testDetails.toString());
	}
	
	public static class BuildInformationBuilder {
		
		private SQLBuildData build = new SQLBuildData();
		
		public BuildInformationBuilder setId(long id) { build.id = id; return this; }
		public BuildInformationBuilder setServerId(String serverId) { build.serverId = serverId; return this; }
		public BuildInformationBuilder setBuildString(String str) { build.buildString = str; return this; }
		public BuildInformationBuilder setTestString(String str) { build.testString = str; return this; }
		public BuildInformationBuilder setCompletedTime(long time) { build.completedTime = time; return this; }
		public BuildInformationBuilder setCompileTime(double time) { build.compileTime = time; return this; }
		public BuildInformationBuilder setBuildState(BuildState buildState) { build.buildState = buildState; return this; }
		public BuildInformationBuilder setTestDetails(TestDetails testDetails) { build.testDetails = testDetails; return this; }
		public SQLBuildData build() {
			SQLBuildData ret = build;
			build = new SQLBuildData();
			return ret;
		}
		
	}
	
}
