/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.deployment;

import com.projectswg.common.data.PSWGData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedDeploymentData.DeploymentState;
import com.projectswg.lightspeed.StarshipProcess;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.server.ServerServerData;

public class ServerDeploymentData {
	
	private final SharedDeploymentData shared;
	private final ServerBuildData build;
	private final PSWGData pswgData;
	private final int adminServerPort;
	private StarshipProcess process;
	
	public ServerDeploymentData(long id, ServerBuildData build) {
		this.shared = new SharedDeploymentData(id, build.getShared());
		this.build = build;
		this.process = null;
		this.pswgData = new PSWGData();
		this.adminServerPort = (int) (40000 + (id % 100));
		build.getServer().addDeployment(this);
	}
	
	public SharedDeploymentData getShared() {
		return shared;
	}
	
	public ServerBuildData getBuild() {
		return build;
	}
	
	public ServerServerData getServer() {
		return build == null ? null : build.getServer();
	}
	
	public long getId() {
		return shared.getId();
	}
	
	public StarshipProcess getProcess() {
		return process;
	}
	
	public int getAdminServerPort() {
		return adminServerPort;
	}
	
	public long getStartTime() {
		return shared.getStartTime();
	}
	
	public long getEndTime() {
		return shared.getEndTime();
	}
	
	public DeploymentState getDeploymentState() {
		return shared.getDeploymentState();
	}
	
	public boolean isAlive() {
		return shared.getEndTime() == 0;
	}
	
	public PSWGData getPswgData() {
		return pswgData;
	}
	
	public void setProcess(StarshipProcess process) {
		this.process = process;
	}
	
	public void setStartTime(long startTime) {
		shared.setStartTime(startTime);
	}
	
	public void setEndTime(long endTime) {
		shared.setEndTime(endTime);
	}
	
	public void setDeploymentState(DeploymentState state) {
		shared.setDeploymentState(state);
	}
	
}
