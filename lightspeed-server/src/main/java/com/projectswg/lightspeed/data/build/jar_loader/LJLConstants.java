/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build.jar_loader;

public class LJLConstants {
	
	protected static final String REDIRECTED_CLASS_PATH_MANIFEST_NAME  = "Rsrc-Class-Path";
	protected static final String REDIRECTED_MAIN_CLASS_MANIFEST_NAME  = "Rsrc-Main-Class";
	protected static final String DEFAULT_REDIRECTED_CLASSPATH         = "";
	protected static final String MAIN_METHOD_NAME                     = "main";
	protected static final String JAR_INTERNAL_URL_PROTOCOL_WITH_COLON = "jar:rsrc:";
	protected static final String JAR_INTERNAL_SEPARATOR               = "!/";
	protected static final String INTERNAL_URL_PROTOCOL_WITH_COLON     = "rsrc:";
	protected static final String INTERNAL_URL_PROTOCOL                = "rsrc";
	protected static final String PATH_SEPARATOR                       = "/";
	protected static final String CURRENT_DIR                          = "./";
	protected static final String UTF8_ENCODING                        = "UTF-8";
	
}
