/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.info;

import com.projectswg.common.data.info.Config;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.log_wrapper.FileLogWrapper;

import java.io.File;
import java.io.IOException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicBoolean;

public class HeavyweightDataManager implements DataManager {
	
	private final AtomicBoolean initialized;
	private final BackendData backendData;
	private Map<ConfigFile, Config> config;
	
	public HeavyweightDataManager() {
		backendData = new HeavyweightBackendData();
		initialized = new AtomicBoolean(false);
	}
	
	@Override
	public synchronized void initialize() {
		if (initialized.getAndSet(true))
			return;
		initializeConfig();
		if (getConfig(ConfigFile.LIGHTSPEED).getBoolean("ENABLE-LOGGING", true)) {
			try {
				Log.addWrapper(new FileLogWrapper(new File("log.txt")));
			} catch (IOException e) {
				Log.e(e);
			}
		}
	}
	
	@Override
	public synchronized void terminate() {
		if (!initialized.getAndSet(false))
			return;
	}
	
	private synchronized void initializeConfig() {
		config = new ConcurrentHashMap<ConfigFile, Config>();
		for (ConfigFile file : ConfigFile.values()) {
			File f = new File(file.getFilename());
			if (!createFilesAndDirectories(f)) {
				Log.w("DataManager", "ConfigFile could not be loaded! " + file.getFilename());
			} else {
				config.put(file, new Config(f));
			}
		}
	}
	
	private boolean createFilesAndDirectories(File file) {
		if (file.exists())
			return true;
		try {
			String parentName = file.getParent();
			if (parentName != null && !parentName.isEmpty()) {
				File parent = new File(file.getParent());
				if (!parent.exists() && !parent.mkdirs())
					Log.e(getClass().getSimpleName(), "Failed to create parent directories for config: " + file.getCanonicalPath());
			}
		} catch (IOException e) {
			Log.e(e);
		}
		try {
			if (!file.createNewFile())
				Log.e(getClass().getSimpleName(), "Failed to create new config: " + file.getCanonicalPath());
		} catch (IOException e) {
			Log.e(e);
		}
		return file.exists();
	}
	
	/**
	 * Gets the config object associated with a certain file, or NULL if the file failed to load on
	 * startup
	 * 
	 * @param file the file to get the config for
	 * @return the config object associated with the file, or NULL if the config failed to load
	 */
	@Override
	public Config getConfig(ConfigFile file) {
		Config c = config.get(file);
		if (c == null)
			return new Config(file.getFilename());
		return c;
	}
	
	@Override
	public BackendData getBackendData() {
		return backendData;
	}
	
	public boolean isInitialized() {
		return initialized.get();
	}
	
}
