/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build.jar_loader;

import java.io.IOException;
import java.io.InputStream;
import java.lang.reflect.Method;
import java.net.URL;
import java.net.URLClassLoader;
import java.util.Enumeration;
import java.util.jar.Attributes;
import java.util.jar.JarFile;
import java.util.jar.Manifest;

public class LightspeedJarLoader {
	
	public static void main(String[] args) throws Exception {
		ManifestInfo mi = getManifestInfo();
		URL.setURLStreamHandlerFactory(new LJLURLStreamHandlerFactory(Thread.currentThread().getContextClassLoader()));
		URL[] rsrcUrls = new URL[mi.rsrcClassPath.length];
		for (int i = 0; i < mi.rsrcClassPath.length; i++) {
			String rsrcPath = mi.rsrcClassPath[i];
			if (rsrcPath.endsWith(LJLConstants.PATH_SEPARATOR))
				rsrcUrls[i] = new URL(LJLConstants.INTERNAL_URL_PROTOCOL_WITH_COLON + rsrcPath);
			else
				rsrcUrls[i] = new URL(LJLConstants.JAR_INTERNAL_URL_PROTOCOL_WITH_COLON + rsrcPath + LJLConstants.JAR_INTERNAL_SEPARATOR);
		}
		ClassLoader jceClassLoader = new URLClassLoader(rsrcUrls, null);
		Thread.currentThread().setContextClassLoader(jceClassLoader);
		Class<?> c = Class.forName(mi.rsrcMainClass, true, jceClassLoader);
		Method main = c.getMethod(LJLConstants.MAIN_METHOD_NAME, new Class[] {args.getClass()});
		main.invoke((Object) null, new Object[] {args});
	}
	
	private static ManifestInfo getManifestInfo() throws IOException {
		Enumeration<URL> resEnum = Thread.currentThread().getContextClassLoader().getResources(JarFile.MANIFEST_NAME);
		while (resEnum.hasMoreElements()) {
			try {
				URL url = (URL) resEnum.nextElement();
				InputStream is = url.openStream();
				if (is != null) {
					ManifestInfo result = new ManifestInfo();
					Manifest manifest = new Manifest(is);
					Attributes mainAttribs = manifest.getMainAttributes();
					result.rsrcMainClass = mainAttribs.getValue(LJLConstants.REDIRECTED_MAIN_CLASS_MANIFEST_NAME);
					String rsrcCP = mainAttribs.getValue(LJLConstants.REDIRECTED_CLASS_PATH_MANIFEST_NAME);
					if (rsrcCP == null)
						rsrcCP = LJLConstants.DEFAULT_REDIRECTED_CLASSPATH;
					result.rsrcClassPath = rsrcCP.split(" ");
					if ((result.rsrcMainClass != null) && !result.rsrcMainClass.trim().equals(""))
						return result;
				}
			} catch (Exception e) {
				// Silently ignore wrong manifests on classpath?
			}
		}
		System.err.println("Missing attributes for JarRsrcLoader in Manifest (" + LJLConstants.REDIRECTED_MAIN_CLASS_MANIFEST_NAME + ", " + LJLConstants.REDIRECTED_CLASS_PATH_MANIFEST_NAME + ")"); //$NON-NLS-1$ //$NON-NLS-2$ //$NON-NLS-3$
		return null;
	}
	
	private static class ManifestInfo {
		String rsrcMainClass;
		String[] rsrcClassPath;
	}
	
}
