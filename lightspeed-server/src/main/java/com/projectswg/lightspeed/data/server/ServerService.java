/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.server;

import com.projectswg.common.concurrency.PswgBasicScheduledThread;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.LightspeedPacketType;
import com.projectswg.common.network.packets.request.RequestServerDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseServerDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseServerListPacket;
import com.projectswg.lightspeed.control.LightspeedService;
import com.projectswg.lightspeed.data.info.ConfigFile;
import com.projectswg.lightspeed.git.GitInterface;
import com.projectswg.lightspeed.intents.PrepareStarshipIntent;
import com.projectswg.lightspeed.intents.RegisterHttpListenerIntent;
import me.joshlarson.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ServerService extends LightspeedService {
	
	private final Set<ServerServerData> servers;
	private final PswgBasicScheduledThread gitScanner;
	private final GitInterface git;
	
	public ServerService() {
		this.servers = new HashSet<>();
		this.gitScanner = new PswgBasicScheduledThread("git-scanner", this::scanServersForUpdates);
		this.git = new GitInterface();
	}
	
	@Override
	public boolean initialize() {
		servers.addAll(getBackendData().getServerList());
		for (ServerServerData server : servers) {
			server.setLastCommit("?");
		}
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_SERVER_LIST, (type, packet) -> onRequestServerList(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_SERVER_DETAILED, (type, packet) -> onRequestServerDetailed(packet)).broadcast();
		return super.initialize();
	}
	
	@Override
	public boolean start() {
		gitScanner.startWithFixedDelay(15000, 15000);
		return super.start();
	}
	
	@Override
	public boolean stop() {
		gitScanner.stop();
		return super.stop();
	}
	
	private void scanServersForUpdates() {
		for (ServerServerData server : servers) {
			try {
				String commit = git.getLastCommit(server.getDirectory());
				if (commit.isEmpty())
					commit = "?";
				String lastCommit = server.getLastCommit();
				server.setLastCommit(commit);
				if (!lastCommit.equals(commit) && !lastCommit.equals("?") && !commit.equals("?")) {
					if (!git.pullLastUpdate(server.getDirectory(), getConfig(ConfigFile.LIGHTSPEED).getBoolean("GIT-REBASE", false))) {
						Log.e("Failed to pull latest update from %s", server.getName());
						continue;
					}
					Log.i("Server %s was updated. Rebuilding...", server.getName());
					new PrepareStarshipIntent(server, true).broadcast();
				}
			} catch (IOException e) {
				Log.e(e);
			}
		}
	}
	
	private JSONObject onRequestServerList(@SuppressWarnings("unused") Packet packet) {
		synchronized (servers) {
			List<SharedServerData> serverList = new ArrayList<>(servers.size());
			for (ServerServerData server : servers) {
				serverList.add(server.getShared());
			}
			return new ResponseServerListPacket(serverList).getJSON();
		}
	}
	
	private JSONObject onRequestServerDetailed(Packet packet) {
		synchronized (servers) {
			RequestServerDetailedPacket request = (RequestServerDetailedPacket) packet;
			for (ServerServerData server : servers) {
				if (server.getName().equals(request.getServerId())) {
					return new ResponseServerDetailedPacket(server.getShared()).getJSON();
				}
			}
		}
		return null;
	}
	
}
