/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.info;

import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;
import com.projectswg.lightspeed.data.server.ServerServerData;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class LightweightBackendData implements BackendData {
	
	private final Map<String, ServerServerData> serverMap;
	
	public LightweightBackendData() {
		serverMap = new HashMap<>();
	}
	
	@Override
	public ServerServerData getServer(String serverId) {
		return serverMap.get(serverId);
	}
	
	@Override
	public List<ServerServerData> getServerList() {
		return new ArrayList<>(serverMap.values());
	}
	
	public void insertServer(ServerServerData server) {
		serverMap.put(server.getName(), server);
	}
	
	@Override
	public void insertBuild(ServerBuildData build) {
		
	}
	
	@Override
	public void insertDeployment(ServerDeploymentData deployment) {
		
	}
	
	public void removeServer(String serverId) {
		serverMap.remove(serverId);
	}
	
}
