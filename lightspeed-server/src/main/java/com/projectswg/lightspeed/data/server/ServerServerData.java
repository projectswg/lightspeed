/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.server;

import com.projectswg.common.data.SharedServerData;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;

import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

public class ServerServerData {
	
	private final SharedServerData shared;
	private final TreeMap<Long, ServerBuildData> builds;
	private final TreeMap<Long, ServerDeploymentData> deployments;
	
	public ServerServerData(String name) {
		this.shared = new SharedServerData(name);
		this.builds = new TreeMap<>();
		this.deployments = new TreeMap<>();
	}
	
	public SharedServerData getShared() {
		return shared;
	}
	
	public String getName() {
		return shared.getName();
	}
	
	public String getDirectory() {
		return shared.getDirectory();
	}
	
	public String getJvmArguments() {
		return shared.getJvmArguments();
	}
	
	public String getLastCommit() {
		return shared.getLastCommit();
	}
	
	public void setDirectory(String directory) {
		shared.setDirectory(directory);
	}
	
	public void setJvmArguments(String jvmArgs) {
		shared.setJvmArguments(jvmArgs);
	}
	
	public void setLastCommit(String lastCommit) {
		shared.setLastCommit(lastCommit);
	}

	public void addBuild(ServerBuildData build) {
		shared.addBuild(build.getShared());
		builds.put(build.getId(), build);
	}
	
	public void addDeployment(ServerDeploymentData deployment) {
		shared.addDeployment(deployment.getShared());
		deployments.put(deployment.getId(), deployment);
	}
	
	public ServerBuildData getLastBuild() {
		if (builds.isEmpty())
			return null;
		return builds.lastEntry().getValue();
	}
	
	public ServerBuildData getBuild(long buildId) {
		return builds.get(buildId);
	}
	
	public List<ServerBuildData> getBuilds() {
		return new ArrayList<>(builds.values());
	}
	
	public ServerDeploymentData getLastDeployment() {
		if (deployments.isEmpty())
			return null;
		return deployments.lastEntry().getValue();
	}
	
	public ServerDeploymentData getDeployment(long deploymentId) {
		return deployments.get(deploymentId);
	}
	
	public List<ServerDeploymentData> getDeployments() {
		return new ArrayList<>(deployments.values());
	}
	
}
