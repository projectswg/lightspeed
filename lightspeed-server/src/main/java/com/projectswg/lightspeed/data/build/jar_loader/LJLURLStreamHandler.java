/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build.jar_loader;

import java.io.IOException;
import java.net.URL;
import java.net.URLStreamHandler;

public class LJLURLStreamHandler extends URLStreamHandler {
	
	private ClassLoader classLoader;
	
	public LJLURLStreamHandler(ClassLoader classLoader) {
		this.classLoader = classLoader;
	}
	
	protected java.net.URLConnection openConnection(URL u) throws IOException {
		return new LJLURLConnection(u, classLoader);
	}
	
	protected void parseURL(URL url, String spec, int start, int limit) {
		String file;
		if (spec.startsWith(LJLConstants.INTERNAL_URL_PROTOCOL_WITH_COLON))
			file = spec.substring(5);
		else if (url.getFile().equals(LJLConstants.CURRENT_DIR))
			file = spec;
		else if (url.getFile().endsWith(LJLConstants.PATH_SEPARATOR))
			file = url.getFile() + spec;
		else
			file = spec;
		setURL(url, LJLConstants.INTERNAL_URL_PROTOCOL, "", -1, null, null, file, null, null);
	}
}
