/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.sql;

import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;
import me.joshlarson.json.JSONObject;

public class SQLDeploymentData {
	
	private long id;
	private String serverId;
	private long buildId;
	private long startTime;
	private long endTime;
	
	public SQLDeploymentData(ServerDeploymentData deployment) {
		this.id = deployment.getId();
		this.serverId = deployment.getServer().getName();
		this.buildId = deployment.getBuild().getId();
		this.startTime = deployment.getStartTime();
		this.endTime = deployment.getEndTime();
	}
	
	public SQLDeploymentData(JSONObject json) {
		this.id = json.getLong("id");
		this.serverId = json.getString("server_id");
		this.buildId = json.getLong("build_id");
		this.startTime = TimeUtilities.getTimeFromStringUtc(json.getString("start_time"));
		this.endTime = TimeUtilities.getTimeFromStringUtc(json.getString("end_time"));
	}
	
	private SQLDeploymentData() {
		this.id = 0;
		this.serverId = "";
		this.buildId = 0;
		this.startTime = 0;
		this.endTime = 0;
	}
	
	public long getId() {
		return id;
	}
	
	public String getServerId() {
		return serverId;
	}
	
	public long getBuildId() {
		return buildId;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public String toString() {
		return String.format("DeploymentInformation[id=%d, server=%s, build=%d, start_time=%d, end_time=%d]", id, serverId, buildId, startTime, endTime);
	}
	
	public static class DeploymentInformationBuilder {
		
		private SQLDeploymentData deploy = new SQLDeploymentData();
		
		public DeploymentInformationBuilder setId(long id) { deploy.id = id; return this; }
		public DeploymentInformationBuilder setServerId(String id) { deploy.serverId = id; return this; }
		public DeploymentInformationBuilder setBuildId(long id) { deploy.buildId = id; return this; }
		public DeploymentInformationBuilder setStartTime(long time) { deploy.startTime = time; return this; }
		public DeploymentInformationBuilder setEndTime(long time) { deploy.endTime = time; return this; }
		public SQLDeploymentData build() {
			SQLDeploymentData ret = deploy;
			deploy = new SQLDeploymentData();
			return ret;
		}
		
	}
	
}
