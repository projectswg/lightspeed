/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.sql;

import com.projectswg.common.data.info.RelationalDatabase;
import com.projectswg.common.data.info.RelationalServerFactory;
import com.projectswg.common.debug.Log;
import com.projectswg.lightspeed.data.sql.SQLDeploymentData.DeploymentInformationBuilder;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class DeploymentTable implements AutoCloseable {
	
	private static final String ALL_COLUMNS = "id, server_id, build_id, start_time, end_time";
	private static final String INSERT_DEPLOYMENT_SQL = "INSERT INTO deployments ("+ALL_COLUMNS+") VALUES (?, ?, ?, ?, ?)";
	private static final String GET_DEPLOYMENT_BY_ID_SQL = "SELECT "+ALL_COLUMNS+" FROM deployments WHERE id = ?";
	private static final String GET_DEPLOYMENTS_BY_BUILD_SQL = "SELECT "+ALL_COLUMNS+" FROM deployments WHERE build_id = ?";
	private static final String GET_DEPLOYMENTS_BY_SERVER_SQL = "SELECT "+ALL_COLUMNS+" FROM deployments WHERE server_id = ?";
	private static final String GET_LAST_DEPLOYMENT_SQL = "SELECT "+ALL_COLUMNS+" FROM deployments WHERE server_id = ? ORDER BY start_time DESC LIMIT 1";
	
	private final RelationalDatabase database;
	private final PreparedStatement insertDeploymentStatement;
	private final PreparedStatement getDeploymentByIdStatement;
	private final PreparedStatement getDeploymentsByBuildStatement;
	private final PreparedStatement getDeploymentsByServerStatement;
	private final PreparedStatement getLastDeploymentStatement;
	
	public DeploymentTable() {
		this.database = RelationalServerFactory.getServerDatabase("lightspeed.db");
		this.insertDeploymentStatement = database.prepareStatement(INSERT_DEPLOYMENT_SQL);
		this.getDeploymentByIdStatement = database.prepareStatement(GET_DEPLOYMENT_BY_ID_SQL);
		this.getDeploymentsByBuildStatement = database.prepareStatement(GET_DEPLOYMENTS_BY_BUILD_SQL);
		this.getDeploymentsByServerStatement = database.prepareStatement(GET_DEPLOYMENTS_BY_SERVER_SQL);
		this.getLastDeploymentStatement = database.prepareStatement(GET_LAST_DEPLOYMENT_SQL);
	}
	
	public DeploymentTable(RelationalDatabase db) {
		this.database = null;
		this.insertDeploymentStatement = db.prepareStatement(INSERT_DEPLOYMENT_SQL);
		this.getDeploymentByIdStatement = db.prepareStatement(GET_DEPLOYMENT_BY_ID_SQL);
		this.getDeploymentsByBuildStatement = db.prepareStatement(GET_DEPLOYMENTS_BY_BUILD_SQL);
		this.getDeploymentsByServerStatement = db.prepareStatement(GET_DEPLOYMENTS_BY_SERVER_SQL);
		this.getLastDeploymentStatement = db.prepareStatement(GET_LAST_DEPLOYMENT_SQL);
	}
	
	@Override
	public void close() {
		if (database != null)
			database.close();
		safeClose(insertDeploymentStatement);
		safeClose(getDeploymentByIdStatement);
		safeClose(getDeploymentsByBuildStatement);
		safeClose(getDeploymentsByServerStatement);
		safeClose(getLastDeploymentStatement);
	}
	
	public boolean insertDeployment(SQLDeploymentData deployment) {
		try {
			synchronized (insertDeploymentStatement) {
				insertDeploymentStatement.setLong(1, deployment.getId());
				insertDeploymentStatement.setString(2, deployment.getServerId());
				insertDeploymentStatement.setLong(3, deployment.getBuildId());
				insertDeploymentStatement.setLong(4, deployment.getStartTime());
				insertDeploymentStatement.setLong(5, deployment.getEndTime());
				return insertDeploymentStatement.executeUpdate() == 1;
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return false;
	}
	
	public SQLDeploymentData getDeploymentById(long id) {
		try {
			synchronized (getDeploymentByIdStatement) {
				getDeploymentByIdStatement.setLong(1, id);
				ResultSet set = getDeploymentByIdStatement.executeQuery();
				if (set.next())
					return createDeploymentFromSet(set);
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return null;
	}
	
	public SQLDeploymentData getLastDeployment(String serverId) {
		try {
			synchronized (getLastDeploymentStatement) {
				getLastDeploymentStatement.setString(1, serverId);
				ResultSet set = getLastDeploymentStatement.executeQuery();
				if (set.next())
					return createDeploymentFromSet(set);
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return null;
	}
	
	public List<SQLDeploymentData> getDeployments(String serverId) {
		List<SQLDeploymentData> deployments = new ArrayList<>();
		try {
			synchronized (getDeploymentsByServerStatement) {
				getDeploymentsByServerStatement.setString(1, serverId);
				ResultSet set = getDeploymentsByServerStatement.executeQuery();
				while (set.next())
					deployments.add(createDeploymentFromSet(set));
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return deployments;
	}
	
	public List<SQLDeploymentData> getDeployments(long buildId) {
		List<SQLDeploymentData> deployments = new ArrayList<>();
		try {
			synchronized (getDeploymentsByBuildStatement) {
				getDeploymentsByBuildStatement.setLong(1, buildId);
				ResultSet set = getDeploymentsByBuildStatement.executeQuery();
				while (set.next())
					deployments.add(createDeploymentFromSet(set));
			}
		} catch (SQLException e) {
			Log.e(e);
		}
		return deployments;
	}
	
	private SQLDeploymentData createDeploymentFromSet(ResultSet set) throws SQLException {
		return new DeploymentInformationBuilder()
				.setId(set.getLong("id"))
				.setServerId(set.getString("server_id"))
				.setBuildId(set.getLong("build_id"))
				.setStartTime(set.getLong("start_time"))
				.setEndTime(set.getLong("end_time"))
				.build();
	}
	
	private static void safeClose(PreparedStatement statement) {
		try {
			statement.close();
		} catch (SQLException e) {
			// Ignored
		}
	}
	
}
