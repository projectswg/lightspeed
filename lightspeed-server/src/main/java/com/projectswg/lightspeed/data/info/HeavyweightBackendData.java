/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.info;

import com.projectswg.common.data.SharedDeploymentData.DeploymentState;
import com.projectswg.common.data.info.RelationalDatabase;
import com.projectswg.common.data.info.RelationalServerFactory;
import com.projectswg.lightspeed.data.build.ServerBuildData;
import com.projectswg.lightspeed.data.deployment.ServerDeploymentData;
import com.projectswg.lightspeed.data.server.ServerServerData;
import com.projectswg.lightspeed.data.sql.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class HeavyweightBackendData implements BackendData {
	
	private final Map<String, ServerServerData> serverMap;
	
	public HeavyweightBackendData() {
		this.serverMap = new HashMap<>();
		loadServers();
	}
	
	@Override
	public void insertBuild(ServerBuildData build) {
		try (BuildHistoryTable buildTable = new BuildHistoryTable()) {
			buildTable.insertBuildHistory(new SQLBuildData(build));
		}
	}
	
	@Override
	public void insertDeployment(ServerDeploymentData deployment) {
		try (DeploymentTable deploymentTable = new DeploymentTable()) {
			deploymentTable.insertDeployment(new SQLDeploymentData(deployment));
		}
	}
	
	@Override
	public List<ServerServerData> getServerList() {
		List<ServerServerData> servers;
		synchronized (serverMap) {
			servers = new ArrayList<>(serverMap.values());
		}
		return servers;
	}
	
	@Override
	public ServerServerData getServer(String serverId) {
		synchronized (serverMap) {
			ServerServerData server = serverMap.get(serverId);
			if (server == null)
				return null;
			try (RelationalDatabase db = RelationalServerFactory.getServerDatabase("lightspeed.db")) {
				fetchBuildHistory(db, server);
			}
			return server;
		}
	}
	
	private void loadServers() {
		synchronized (serverMap) {
			try (RelationalDatabase db = RelationalServerFactory.getServerDatabase("lightspeed.db")) {
				try (ServerTable serverTable = new ServerTable()) {
					for (String serverId : serverTable.getServerStrings()) {
						ServerServerData server = createServerFromInfo(serverTable.getServerById(serverId));
						fetchBuildHistory(db, server);
						fetchDeploymentHistory(db, server);
						serverMap.put(serverId, server);
					}
				}
			}
		}
	}
	
	private void fetchBuildHistory(RelationalDatabase db, ServerServerData server) {
		try (BuildHistoryTable buildTable = new BuildHistoryTable(db)) {
			for (SQLBuildData info : buildTable.getBuildHistory(server.getName())) {
				ServerBuildData build = createBuildFromInfo(server, info);
				server.addBuild(build);
			}
		}
	}
	
	private void fetchDeploymentHistory(RelationalDatabase db, ServerServerData server) {
		try (DeploymentTable deploymentTable = new DeploymentTable(db)) {
			for (SQLDeploymentData info : deploymentTable.getDeployments(server.getName())) {
				ServerDeploymentData deployment = createDeploymentFromInfo(server, info);
				server.addDeployment(deployment);
			}
		}
	}
	
	private ServerServerData createServerFromInfo(SQLServerData info) {
		ServerServerData server = new ServerServerData(info.getId());
		server.setDirectory(info.getDir());
		server.setJvmArguments(info.getJvmArgs());
		return server;
	}
	
	private ServerBuildData createBuildFromInfo(ServerServerData server, SQLBuildData info) {
		ServerBuildData build = new ServerBuildData(info.getId(), server);
		build.setTime(info.getCompletedTime());
		build.setBuildString(info.getBuildString());
		build.setTestString(info.getTestString());
		build.setCompileTime(info.getCompileTime());
		build.setBuildState(info.getBuildState());
		build.setTestDetails(info.getTestDetails());
		return build;
	}
	
	private ServerDeploymentData createDeploymentFromInfo(ServerServerData server, SQLDeploymentData info) {
		ServerDeploymentData deployment = new ServerDeploymentData(info.getId(), server.getBuild(info.getBuildId()));
		deployment.setStartTime(info.getStartTime());
		deployment.setEndTime(info.getEndTime());
		deployment.setDeploymentState(DeploymentState.SHUTDOWN);
		return deployment;
	}
	
}
