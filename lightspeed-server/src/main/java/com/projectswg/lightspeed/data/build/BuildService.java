/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.info.Config;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.LightspeedPacketType;
import com.projectswg.common.network.packets.post.PostBuildPacket;
import com.projectswg.common.network.packets.post.PostResult;
import com.projectswg.common.network.packets.post.PostStopBuildPacket;
import com.projectswg.common.network.packets.request.RequestBuildDetailedPacket;
import com.projectswg.common.network.packets.request.RequestBuildListPacket;
import com.projectswg.common.network.packets.response.ResponseBuildDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseBuildListPacket;
import com.projectswg.lightspeed.control.LightspeedService;
import com.projectswg.lightspeed.data.build.ServerBuildData.InstallationCallback;
import com.projectswg.lightspeed.data.info.ConfigFile;
import com.projectswg.lightspeed.data.server.ServerServerData;
import com.projectswg.lightspeed.intents.LaunchStarshipIntent;
import com.projectswg.lightspeed.intents.PrepareStarshipIntent;
import com.projectswg.lightspeed.intents.RegisterHttpListenerIntent;
import com.projectswg.lightspeed.sequenced.SequencedStarter;
import me.joshlarson.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.util.*;
import java.util.concurrent.atomic.AtomicLong;

public class BuildService extends LightspeedService {
	
	private final AtomicLong buildId;
	private final Map <String, Build> inprogress;
	private final Set<ServerBuildData> buildHistory;
	
	public BuildService() {
		buildId = new AtomicLong(0);
		inprogress = new HashMap<>();
		buildHistory = new HashSet<>();
		
		registerForIntent(PrepareStarshipIntent.class, this::onPrepareStarshipIntent);
	}
	
	@Override
	public boolean initialize() {
		for (ServerServerData server : getBackendData().getServerList()) {
			buildHistory.addAll(server.getBuilds());
		}
		for (ServerBuildData build : buildHistory) {
			if (build.getId() > buildId.get()) {
				buildId.set(build.getId());
			}
		}
		new RegisterHttpListenerIntent(LightspeedPacketType.POST_BUILD, (type, packet) -> processBuild(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.POST_STOP_BUILD, (type, packet) -> processStopBuild(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_BUILD_LIST, (type, packet) -> processBuildList(packet)).broadcast();
		new RegisterHttpListenerIntent(LightspeedPacketType.REQUEST_BUILD_DETAILED, (type, packet) -> processBuildDetailed(packet)).broadcast();
		return super.initialize();
	}
	
	private File getJdk() {
		Config config = getConfig(ConfigFile.LIGHTSPEED);
		String path = config.getString("jdk", System.getProperty("java.home"));
		if (path.isEmpty()) {
			Log.e("Invalid path for jdk! Empty");
			throw new IllegalStateException("JDK path is invalid!");
		}
		File jdk = new File(path);
		if (!jdk.isDirectory()) {
			Log.e("Invalid path for jdk! Not a directory");
			throw new IllegalStateException("JDK path is invalid!");
		}
		return jdk;
	}
	
	private void onPrepareStarshipIntent(PrepareStarshipIntent psi) {
		ServerBuildData buildData = new ServerBuildData(buildId.incrementAndGet(), psi.getServer());
		Log.i("Creating build #%d", buildData.getId());
		BuildInstallationCallback callback = new BuildInstallationCallback(psi.getServer().getName(), psi.isRedeploy());
		JavaBuildMethod buildMethod = new JavaBuildMethod(getJdk(), psi.getServer(), buildData, callback);
		synchronized (inprogress) {
			Build build = new Build(buildData);
			Build old = inprogress.put(psi.getServer().getName(), build);
			if (old != null) {
				old.setReplacement(build);
				old.stop();
			} else {
				build.start();
			}
		}
		buildMethod.start();
	}
	
	private JSONObject processBuild(Packet packet) {
		PostBuildPacket p = (PostBuildPacket) packet;
		ServerServerData server = getBackendData().getServer(p.getServerId());
		if (server != null)
			new PrepareStarshipIntent(server, false).broadcast();
		return new PostResult(server != null).getJSON();
	}
	
	private JSONObject processStopBuild(Packet packet) {
		PostStopBuildPacket p = (PostStopBuildPacket) packet;
		synchronized (inprogress) {
			Build build = inprogress.get(p.getServerId());
			if (build != null)
				build.stop();
			return new PostResult(build != null).getJSON();
		}
	}
	
	private JSONObject processBuildList(Packet packet) {
		RequestBuildListPacket p = (RequestBuildListPacket) packet;
		List<SharedBuildData> buildList = new ArrayList<>();
		synchronized (buildHistory) {
			for (ServerBuildData build : buildHistory) {
				if (p.getServerId().equals(build.getServer().getName())) {
					buildList.add(build.getShared());
				}
			}
		}
		return new ResponseBuildListPacket(p.getServerId(), buildList).getJSON();
	}
	
	private JSONObject processBuildDetailed(Packet packet) {
		RequestBuildDetailedPacket p = (RequestBuildDetailedPacket) packet;
		synchronized (buildHistory) {
			for (ServerBuildData build : buildHistory) {
				if (p.getBuildId() == build.getId()) {
					return new ResponseBuildDetailedPacket(build.getShared()).getJSON();
				}
			}
		}
		return null;
	}
	
	private class BuildInstallationCallback implements InstallationCallback {
		
		private final String serverId;
		private final boolean redeploy;
		
		public BuildInstallationCallback(String serverId, boolean redeploy) {
			this.serverId = serverId;
			this.redeploy = redeploy;
		}
		
		@Override
		public void onCompleted(ServerBuildData build) {
			removeInProgress();
			ServerServerData server = getBackendData().getServer(serverId);
			if (server == null) {
				Log.e("Build Success - error while retrieving server information. Server: %s", serverId);
				return;
			}
			if (build.getBuildState() == BuildState.SUCCESS) {
				if (build.getJar() != null) {
					try {
						File releases = new File("data/releases");
						releases.mkdirs();
						File jar = new File(releases, serverId+".jar");
						Files.copy(build.getJar().toPath(), jar.toPath(), StandardCopyOption.REPLACE_EXISTING);
						Log.i("Build Success! Server: %s", build.getServer().getName());
						new LaunchStarshipIntent(build, redeploy).broadcast();
					} catch (IOException e) {
						Log.e(e);
					}
				}
			} else {
				Log.e("Build Failed! Server: %s  State: %s", build.getServer().getName(), build.getBuildState());
				Log.e("Output:%n%s", build.getTestString());
			}
		}
		
		@Override
		public void onCancelled(ServerBuildData build) {
			removeInProgress();
			Log.i("Build cancelled: %s", serverId);
		}
		
		private void removeInProgress() {
			synchronized (inprogress) {
				Build build = inprogress.remove(serverId);
				if (build == null)
					return;
				getBackendData().insertBuild(build.get());
			}
		}
		
	}
	
	private class Build extends SequencedStarter {
		
		private final ServerBuildData build;
		
		public Build(ServerBuildData build) {
			this.build = build;
		}
		
		public ServerBuildData get() {
			return build;
		}
		
		@Override
		protected void onStart() {
			synchronized (buildHistory) {
				buildHistory.add(build);
			}
			Log.i("Building %s", build.getServer().getName());
		}
		
		@Override
		protected void onStop() {
			if (build.getBuildState() != BuildState.SUCCESS)
				build.setBuildState(BuildState.CANCELLED);
		}
		
	}
	
}
