/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.sql;

import com.projectswg.common.data.CsvTable;
import com.projectswg.common.debug.Log;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ServerTable implements AutoCloseable {
	
	private final CsvTable table;
	
	public ServerTable() {
		this.table = new CsvTable(new File("data/servers.csv"));
		try {
			this.table.read();
		} catch (IOException e) {
			Log.e(e);
		}
	}
	
	public void close() {
		
	}
	
	public SQLServerData getServerById(String id) {
		for (int i = 0; i < table.getRows(); i++) {
			if (table.getRecord(i, "name").equals(id)) {
				return new SQLServerData(id, table.getRecord(i, "directory"), table.getRecord(i, "jvm_arguments"));
			}
		}
		return null;
	}
	
	public List<String> getServerStrings() {
		List<String> servers = new ArrayList<>();
		for (int i = 0; i < table.getRows(); i++) {
			servers.add(table.getRecord(i, "name"));
		}
		return servers;
	}
	
}
