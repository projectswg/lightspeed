/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build;

import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.debug.Log;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed.data.build.ServerBuildData.InstallationCallback;
import com.projectswg.lightspeed.data.server.ServerServerData;
import org.gradle.tooling.GradleConnector;
import org.gradle.tooling.ProjectConnection;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.UnsupportedEncodingException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class JavaBuildMethod {
	
	private static final Pattern JUNIT_PATTERN = Pattern.compile(".*Tests run: \\((\\d+)\\), Failures: \\((\\d+)\\).*", Pattern.DOTALL);
	
	private final ServerBuildData buildData;
	private final InstallationCallback callback;
	private final ProjectConnection connection;
	
	public JavaBuildMethod(File jdk, ServerServerData serverData, ServerBuildData buildData, InstallationCallback callback) {
		this.buildData = buildData;
		this.callback = callback;
		this.connection = GradleConnector.newConnector().forProjectDirectory(new File(serverData.getDirectory())).connect();
	}
	
	public void start() {
		new Thread(this::createJar, "java-build-"+buildData.getId()).start();
	}
	
	private void createJar() {
		try {
			buildData.setTime(TimeUtilities.getTime());
			if (build() && test() && install()) {
				updateState(BuildState.SUCCESS);
			} else {
				if (buildData.getBuildState() != BuildState.CANCELLED) {
					updateState(BuildState.CANCELLED);
				}
			}
		} catch (Exception e) {
			Log.e(e);
			updateState(BuildState.FAILED);
		}
	}
	
	private boolean build() {
		updateState(BuildState.COMPILING);
		long start = System.nanoTime();
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		boolean success = true;
		try {
			connection.newBuild().forTasks("compileJava").setStandardOutput(output).setStandardError(output).run();
		} catch (Exception e) {
			success = false;
		}
		buildData.setBuildString(output.toString());
		buildData.setCompileTime((System.nanoTime() - start) / 1E6);
		if (!success)
			updateState(BuildState.COMPILE_FAILED);
		return success && !detectCancelled(buildData, callback);
	}
	
	private boolean test() {
		updateState(BuildState.TESTING);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		boolean success = true;
		try {
			connection.newBuild().setJvmArguments("-Duser.dir="+buildData.getServer().getDirectory()).forTasks("cleanTest", "test").setStandardOutput(output).setStandardError(output).run();
		} catch (Exception e) {
			success = false;
		}
		String outputString = getOutputStreamString(output);
		Matcher m = JUNIT_PATTERN.matcher(outputString);
		if (m.matches())
			buildData.setTestDetails(new TestDetails(Integer.parseInt(m.group(1)), Integer.parseInt(m.group(2))));
		else
			buildData.setTestDetails(new TestDetails(0, 0));
		Log.d("Tests Completed. Total: %d  Failed: %d", buildData.getTestDetails().getTotal(), buildData.getTestDetails().getFailures());
		buildData.setTestString(outputString);
		if (!success)
			updateState(BuildState.TEST_FAILED);
		return success && !detectCancelled(buildData, callback);
	}
	
	private boolean install() {
		updateState(BuildState.INSTALLING);
		ByteArrayOutputStream output = new ByteArrayOutputStream();
		boolean success = true;
		try {
			connection.newBuild().setJvmArguments("-Duser.dir="+buildData.getServer().getDirectory()).forTasks("shadowJar").setStandardOutput(output).setStandardError(output).run();
		} catch (Exception e) {
			success = false;
		}
		if (!success)
			updateState(BuildState.INSTALL_FAILED);
		buildData.setJar(new File(buildData.getServer().getDirectory(), "build/libs/Holocore.jar"));
		return success && !detectCancelled(buildData, callback);
	}
	
	private String getOutputStreamString(ByteArrayOutputStream output) {
		try {
			return output.toString("ASCII");
		} catch (UnsupportedEncodingException e) {
			Log.e(e);
			return "";
		}
	}
	
	private boolean detectCancelled(ServerBuildData buildData, InstallationCallback callback) {
		if (buildData.getBuildState() == BuildState.CANCELLED) {
			updateState(BuildState.CANCELLED);
			return true;
		}
		return false;
	}
	
	private void updateState(BuildState newState) {
		buildData.setBuildState(newState);
		if (callback != null && newState == BuildState.CANCELLED)
			callback.onCancelled(buildData);
		else if (callback != null && (newState == BuildState.SUCCESS || newState == BuildState.FAILED))
			callback.onCompleted(buildData);
	}
	
}
