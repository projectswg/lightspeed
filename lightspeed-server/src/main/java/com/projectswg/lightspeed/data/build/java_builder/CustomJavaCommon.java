/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build.java_builder;

import com.projectswg.common.debug.Log;

import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;


public class CustomJavaCommon {
	
	protected static final String SEP     = File.separator;
	protected static final String OUT_JAR = "bin"+SEP+"ProjectSWG.jar";
	protected static final String SRC_DIR = "src";
	protected static final String TST_DIR = "test";
	protected static final String BIN_DIR = "bin";
	protected static final String LIB_DIR = "lib";
	
	public static int execute(String [] command, File dir) {
		return execute(command, dir, false);
	}
	
	public static int execute(String [] command, File dir, boolean output) {
		return execute(command, dir, null, output);
	}
	
	public static int execute(String [] command, File dir, StringBuilder out, boolean output) {
		try {
			Log.i("Exec %s", Arrays.toString(command));
			Process process = new ProcessBuilder(command).directory(dir).redirectErrorStream(true).start();
			if (out == null)
				ingestOutput(process.getInputStream(), output);
			else
				out.append(getOutput(process.getInputStream()));
			return process.waitFor();
		} catch (IOException e) {
			Log.e(e);
		} catch (InterruptedException e) {
			
		}
		return -1;
	}
	
	public static String getOutput(InputStream is) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		StringBuilder builder = new StringBuilder();
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				builder.append(line + '\n');
			}
		} catch (IOException e) {
			Log.e(e);
		}
		return builder.toString();
	}
	
	public static void ingestOutput(InputStream is, boolean output) {
		BufferedReader reader = new BufferedReader(new InputStreamReader(is));
		String line;
		try {
			while ((line = reader.readLine()) != null) {
				if (output)
					System.out.println(line);
			}
		} catch (IOException e) {
			Log.e(e);
		}
	}
	
	public static String [] merge(String [] left, String [] right) {
		String [] ret = new String[left.length + right.length];
		System.arraycopy(left, 0, ret, 0, left.length);
		System.arraycopy(right, 0, ret, left.length, right.length);
		return ret;
	}
	
	public static String getFiles(String repo, String base, String extension, char delim) {
		return getJavaFiles(base+repo, extension, delim).replace(base, "");
	}
	
	public static String getJavaFiles(String repo, String extension, char delim) {
		File dir = new File(repo);
		String ret = "";
		for (String file : getJavaFilesRecursive(extension, dir)) {
			ret += file + delim;
		}
		if (ret.isEmpty())
			return ret;
		return ret.substring(0, ret.length()-1);
	}
	
	public static List<String> getJavaFilesRecursive(String extension, File dir) {
		List<String> files = new ArrayList<>();
		for (String subStr : dir.list()) {
			File sub = new File(dir, subStr);
			if (sub.isDirectory()) {
				files.addAll(getJavaFilesRecursive(extension, sub));
			} else if (sub.getName().endsWith(extension)) {
				files.add(sub.getAbsolutePath());
			}
		}
		return files;
	}
	
}
