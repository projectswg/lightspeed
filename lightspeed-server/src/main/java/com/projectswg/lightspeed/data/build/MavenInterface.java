/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed.data.build;

import com.projectswg.common.debug.Log;
import com.projectswg.lightspeed.data.build.ServerBuildData.InstallationCallback;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class MavenInterface implements BuildMethod {
	
	private final File maven;
	private boolean offline;
	private String version;
	
	public MavenInterface(File maven) {
		this.maven = maven;
		if (!maven.isFile())
			throw new IllegalArgumentException("File does not exist!");
		initializeMaven();
		this.offline = false;
	}
	
	public String getMavenVersion() {
		return version;
	}
	
	public void setOffline(boolean offline) {
		this.offline = offline;
	}
	
	public boolean isOffline() {
		return offline;
	}
	
	public ServerBuildData createJar(String repo, InstallationCallback callback) {
//		return ServerBuildData.install(maven, new File(repo), offline, callback);
		return null;
	}
	
	private boolean initializeMaven() {
		String version = execute(null, "-v");
		Pattern r = Pattern.compile("Apache Maven[ *]([\\d+].[\\d+].[\\d+])");
		Matcher m = r.matcher(version);
		this.version = null;
		if (!m.find()) {
			return false;
		}
		this.version = m.group(1);
		return true;
	}
	
	private String execute(File directory, String arguments) {
		Process p;
		try {
			p = new ProcessBuilder(maven.getAbsolutePath(), arguments).directory(directory).redirectErrorStream(true).start();
			BufferedReader input = new BufferedReader(new InputStreamReader(p.getInputStream()));
			String line;
			StringBuilder builder = new StringBuilder();
			while ((line = input.readLine()) != null)
				builder.append(line + '\n');
			return builder.toString();
		} catch (IOException e) {
			Log.e(e);
			return null;
		}
	}
	
}
