/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed;

import com.projectswg.common.control.IntentManager;
import com.projectswg.common.control.Manager;
import com.projectswg.common.data.info.RelationalServerFactory;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.Log.LogLevel;
import com.projectswg.common.debug.ThreadPrinter;
import com.projectswg.common.debug.log_wrapper.ConsoleLogWrapper;
import com.projectswg.common.debug.log_wrapper.FileLogWrapper;
import com.projectswg.lightspeed.data.info.DataManager;
import com.projectswg.lightspeed.data.info.HeavyweightDataManager;

import java.io.File;
import java.util.concurrent.atomic.AtomicBoolean;

public class Lightspeed {
	
	private static final AtomicBoolean SHUTDOWN_HOOK = new AtomicBoolean(false);
	
	private final IntentManager intentManager;
	private final DataManager dataManager;
	
	public Lightspeed() {
		this.intentManager = new IntentManager(Runtime.getRuntime().availableProcessors());
		this.dataManager = new HeavyweightDataManager();
	}
	
	public void initialize() {
		intentManager.initialize();
		dataManager.initialize();
		
		IntentManager.setInstance(intentManager);
		LightspeedMainManager.setDataManager(dataManager);
	}
	
	public void run() {
		Manager.startManager(new LightspeedMainManager());
	}
	
	public void terminate() {
		dataManager.terminate();
		intentManager.terminate();
	}
	
	public static void main(String [] args) throws InterruptedException {
		ThreadGroup group = new ThreadGroup("lightspeed");
		Thread mainThread = new Thread(group, Lightspeed::mainThread, "main");
		Runtime.getRuntime().addShutdownHook(createShutdownHook(mainThread));
		
		mainThread.start();
		mainThread.join();
		
		if (!SHUTDOWN_HOOK.get())
			System.exit(0);
	}
	
	private static void mainThread() {
		try {
			Log.addWrapper(new ConsoleLogWrapper(LogLevel.VERBOSE));
			Log.addWrapper(new FileLogWrapper(new File("log.txt")));
			RelationalServerFactory.setBasePath("data/");
			
			Lightspeed lightspeed = new Lightspeed();
			lightspeed.initialize();
			lightspeed.run();
			lightspeed.terminate();
			
			ThreadPrinter.printActiveThreads();
		} catch (Throwable t) {
			Log.e(t);
		}
	}
	
	private static Thread createShutdownHook(Thread mainThread) {
		Thread currentThread = Thread.currentThread();
		Thread thread = new Thread(() -> {
			SHUTDOWN_HOOK.set(true);
			currentThread.interrupt();
			mainThread.interrupt();
			try {
				mainThread.join();
			} catch (InterruptedException e) {
				Log.e(e);
			}
		}, "lightspeed-shutdown-hook");
		thread.setDaemon(true);
		return thread;
	}
	
}
