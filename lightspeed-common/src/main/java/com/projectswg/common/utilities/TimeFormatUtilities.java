/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.utilities;

public class TimeFormatUtilities {
	
	public static String getTime(long time) {
		int days = (int) (time / (24 * 3600E3));
		int hours = (int) (time % (24 * 3600E3) / 3600E3);
		int minutes = (int) ((time % 3600E3) / 60E3);
		int seconds = (int) (time % 60E3) / 1000;
		int milli = (int) (time % 1000);
		String text = "";
		text += getPlurality("Day", days);
		text += getPlurality("Hour", hours);
		text += getPlurality("Minute", minutes);
		text += getPlurality("Second", seconds);
		text += getPlurality("Milliseconds", milli);
		return text;
	}
	
	private static String getPlurality(String base, int amount) {
		if (amount > 1)
			return amount + " " + base + "s ";
		if (amount == 1)
			return amount + " " + base + " ";
		return "";
	}
	
}
