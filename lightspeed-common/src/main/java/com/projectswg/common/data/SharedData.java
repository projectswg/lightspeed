/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.data;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class SharedData {
	
	private final Set<SharedServerData> servers;
	private SharedServerData primaryServer;
	
	public SharedData() {
		servers = new HashSet<>();
	}
	
	public void addServer(SharedServerData data) {
		synchronized (servers) {
			if (!servers.add(data))
				return;
		}
		updatePrimaryServer();
	}
	
	public void removeServer(SharedServerData data) {
		synchronized (servers) {
			if (!servers.remove(data))
				return;
		}
		updatePrimaryServer();
	}
	
	public void removeServer(String name) {
		SharedServerData server = getServer(name);
		if (server != null)
			removeServer(server);
	}
	
	public List<SharedServerData> getServers() {
		synchronized (servers) {
			return new ArrayList<>(servers);
		}
	}
	
	public SharedServerData getServer(String name) {
		synchronized (servers) {
			for (SharedServerData server : servers) {
				if (server.getName().equals(name)) {
					return server;
				}
			}
		}
		return null;
	}
	
	public SharedServerData getPrimaryServer() {
		synchronized (servers) {
			return primaryServer;
		}
	}
	
	public void clearServers() {
		synchronized (servers) {
			servers.clear();
		}
	}
	
	private void updatePrimaryServer() {
		synchronized (servers) {
			SharedServerData bestPrimary = null;
			for (SharedServerData server : servers) {
				if (bestPrimary == null || bestPrimary.getName().compareTo(server.getName()) > 0) {
					bestPrimary = server;
				}
			}
			if (primaryServer == null || bestPrimary == null || !bestPrimary.equals(primaryServer)) {
				primaryServer = bestPrimary;
			}
		}
	}
	
}
