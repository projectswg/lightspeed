/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.data;

public class SharedBuildData {
	
	private static final TestDetails DEFAULT_TEST_DETAILS = new TestDetails(0, 0);
	
	private final long id;
	private SharedServerData server;
	private long time;
	private String buildString;
	private String testString;
	private double compileTime;
	private BuildState state;
	private TestDetails testDetails;
	
	public SharedBuildData(long id, SharedServerData server) {
		this.id = id;
		this.server = server;
		this.time = 0;
		this.buildString = "";
		this.testString = "";
		this.compileTime = 0;
		this.state = BuildState.CREATED;
		this.testDetails = DEFAULT_TEST_DETAILS;
	}
	
	public long getId() {
		return id;
	}
	
	public SharedServerData getServer() {
		return server;
	}
	
	public long getTime() {
		return time;
	}
	
	public String getBuildString() {
		return buildString;
	}
	
	public String getTestString() {
		return testString;
	}
	
	public double getCompileTime() {
		return compileTime;
	}
	
	public BuildState getBuildState() {
		return state;
	}
	
	public TestDetails getTestDetails() {
		return testDetails;
	}
	
	public void setServer(SharedServerData server) {
		this.server = server;
	}
	
	public void setTime(long time) {
		this.time = time;
	}
	
	public void setBuildString(String buildString) {
		this.buildString = buildString;
	}
	
	public void setTestString(String testString) {
		this.testString = testString;
	}
	
	public void setCompileTime(double compileTime) {
		this.compileTime = compileTime;
	}
	
	public void setBuildState(BuildState buildState) {
		this.state = buildState;
	}
	
	public void setTestDetails(TestDetails testDetails) {
		this.testDetails = testDetails;
	}
	
	public int hashCode() {
		return Long.hashCode(id);
	}
	
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SharedBuildData))
			return super.equals(o);
		return ((SharedBuildData) o).getId() == id;
	}
	
	public String toString() {
		return String.format("SharedBuildData[id=%d, server=%s]", id, getServer().getName());
	}
	
	public enum BuildState {
		UNKNOWN,
		CREATED,
		COMPILING,
		TESTING,
		INSTALLING,
		COMPILE_FAILED,
		TEST_FAILED,
		INSTALL_FAILED,
		FAILED,
		CANCELLED,
		SUCCESS;
		
		public boolean isInProgress() {
			return this == COMPILING || this == TESTING || this == INSTALLING;
		}
		
		public boolean isFailed() {
			return this == COMPILE_FAILED || this == TEST_FAILED || this == INSTALL_FAILED || this == FAILED;
		}
		
		public boolean isCancelled() {
			return this == CANCELLED;
		}
		
		public boolean isSuccess() {
			return this == SUCCESS;
		}
		
		private static final BuildState [] VALUES = values();
		
		public static BuildState getState(String str) {
			for (BuildState val : VALUES) {
				if (val.name().equalsIgnoreCase(str))
					return val;
			}
			return UNKNOWN;
		}
	}
	
}
