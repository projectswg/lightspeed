/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.data;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.TreeMap;

public class SharedServerData {
	
	private final String name;
	private final TreeMap<Long, SharedBuildData> builds;
	private final TreeMap<Long, SharedDeploymentData> deployments;
	private String directory;
	private String jvmArgs;
	private String lastCommit;
	
	public SharedServerData(String name) {
		this.name = name;
		this.builds = new TreeMap<>();
		this.deployments = new TreeMap<>();
		this.directory = "";
		this.jvmArgs = "";
		this.lastCommit = "";
	}
	
	public String getName() {
		return name;
	}
	
	public String getDirectory() {
		return directory;
	}
	
	public String getJvmArguments() {
		return jvmArgs;
	}
	
	public String getLastCommit() {
		return lastCommit;
	}
	
	public List<SharedBuildData> getBuilds() {
		synchronized (builds) {
			List<SharedBuildData> buildList = new ArrayList<>(builds.values());
			Collections.reverse(buildList);
			return buildList;
		}
	}
	
	public List<SharedDeploymentData> getDeployments() {
		synchronized (deployments) {
			List<SharedDeploymentData> deploymentList = new ArrayList<>(deployments.values());
			Collections.reverse(deploymentList);
			return deploymentList;
		}
	}
	
	public SharedBuildData getBuildData(long id) {
		synchronized (builds) {
			return builds.get(id);
		}
	}
	
	public SharedDeploymentData getDeploymentData(long id) {
		synchronized (deployments) {
			return deployments.get(id);
		}
	}
	
	public SharedBuildData getLastBuild() {
		synchronized (builds) {
			if (builds.isEmpty())
				return null;
			return builds.lastEntry().getValue();
		}
	}
	
	public SharedDeploymentData getLastDeployment() {
		synchronized (deployments) {
			if (deployments.isEmpty())
				return null;
			return deployments.lastEntry().getValue();
		}
	}
	
	public void addBuild(SharedBuildData build) {
		synchronized (builds) {
			build.setServer(this);
			builds.put(build.getId(), build);
		}
	}
	
	public void removeBuild(SharedBuildData build) {
		synchronized (builds) {
			builds.remove(build.getId());
		}
	}
	
	public void removeBuild(long id) {
		SharedBuildData build;
		synchronized (builds) {
			build = builds.get(id);
		}
		if (build != null)
			removeBuild(build);
	}
	
	public void addDeployment(SharedDeploymentData deployment) {
		synchronized (deployments) {
			deployments.put(deployment.getId(), deployment);
		}
	}
	
	public void removeDeployment(SharedDeploymentData deployment) {
		synchronized (deployments) {
			deployments.remove(deployment.getId());
		}
	}
	
	public void removeDeployment(long id) {
		synchronized (deployments) {
			SharedDeploymentData deployment = deployments.get(id);
			if (deployment != null)
				removeDeployment(deployment);
		}
	}
	
	public void setDirectory(String directory) {
		this.directory = directory;
	}
	
	public void setJvmArguments(String jvmArgs) {
		this.jvmArgs = jvmArgs;
	}
	
	public void setLastCommit(String lastCommit) {
		this.lastCommit = lastCommit;
	}
	
	@Override
	public int hashCode() {
		return name.hashCode();
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SharedServerData))
			return super.equals(o);
		return ((SharedServerData) o).getName().equals(name);
	}
	
	public String toString() {
		return String.format("SharedServerData[name=%s, dir=%s, args=%s]", name, directory, jvmArgs);
	}
	
}
