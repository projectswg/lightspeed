/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.data;

public class SharedDeploymentData {
	
	private final long id;
	private SharedBuildData build;
	private long startTime;
	private long endTime;
	private StringBuilder output;
	private DeploymentState state;
	
	public SharedDeploymentData(long id, SharedBuildData build) {
		this.id = id;
		this.build = build;
		this.startTime = 0;
		this.endTime = 0;
		this.output = new StringBuilder("");
		this.state = DeploymentState.CREATED;
	}
	
	public long getId() {
		return id;
	}
	
	public SharedServerData getServer() {
		if (build == null)
			return null;
		return build.getServer();
	}
	
	public SharedBuildData getBuild() {
		return build;
	}
	
	public long getStartTime() {
		return startTime;
	}
	
	public long getEndTime() {
		return endTime;
	}
	
	public String getOutput() {
		return output.toString();
	}
	
	public DeploymentState getDeploymentState() {
		return state;
	}
	
	public void setBuild(SharedBuildData build) {
		this.build = build;
	}
	
	public void setStartTime(long startTime) {
		this.startTime = startTime;
	}
	
	public void setEndTime(long endTime) {
		this.endTime = endTime;
	}
	
	public void setOutput(String output) {
		this.output.setLength(0);
		this.output.append(output);
	}
	
	public void appendOutput(String line) {
		this.output.append(line);
	}
	
	public void setDeploymentState(DeploymentState state) {
		this.state = state;
	}
	
	@Override
	public int hashCode() {
		return Long.hashCode(id);
	}
	
	@Override
	public boolean equals(Object o) {
		if (o == null)
			return false;
		if (!(o instanceof SharedDeploymentData))
			return super.equals(o);
		return ((SharedDeploymentData) o).getId() == id;
	}
	
	@Override
	public String toString() {
		return String.format("SharedDeploymentData[id=%d, build=%d, server=%s, start_time=%d, end_time=%d]", id, getBuild().getId(), getServer().getName(), startTime, endTime);
	}
	
	public enum DeploymentState {
		CREATED,
		UNKNOWN,
		RUNNING,
		SHUTTING_DOWN,
		SHUTDOWN;
		
		private static final DeploymentState [] VALUES = values();
		
		public static DeploymentState getState(String str) {
			for (DeploymentState val : VALUES) {
				if (val.name().equalsIgnoreCase(str))
					return val;
			}
			return UNKNOWN;
		}
	}
	
}
