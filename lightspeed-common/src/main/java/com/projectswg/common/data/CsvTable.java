/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.data;

import com.projectswg.common.debug.Log;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

public class CsvTable {
	
	private final File file;
	private final Map<String, Integer> columns;
	private final List<String[]> table;
	
	public CsvTable(File file) {
		this.file = file;
		this.columns = new HashMap<>();
		this.table = new ArrayList<>();
	}
	
	public void read() throws IOException {
		columns.clear();
		table.clear();
		try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
			readColumns(reader.readLine());
			String line;
			while ((line = reader.readLine()) != null) {
				readRow(line);
			}
		}
	}
	
	public String getRecord(int row, String column) {
		return getRecord(row, columns.get(column));
	}
	
	public String getRecord(int row, int column) {
		return table.get(row)[column];
	}
	
	public int getRows() {
		return table.size();
	}
	
	public int getColumns() {
		return columns.size();
	}
	
	private void readColumns(String line) {
		int i = 0;
		for (String col : line.split(",")) {
			columns.put(col, i);
			i++;
		}
	}
	
	private void readRow(String line) {
		String [] row = line.split(",", columns.size());
		if (row.length != columns.size())
			Log.w("CSV Row does not contain enough columns! %s", Arrays.toString(row));
		table.add(row);
	}
	
}
