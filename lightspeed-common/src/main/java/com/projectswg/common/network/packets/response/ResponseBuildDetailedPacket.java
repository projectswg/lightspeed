/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets.response;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.network.packets.LightspeedPacketType;
import me.joshlarson.json.JSONObject;

public class ResponseBuildDetailedPacket extends ResponsePacket {
	
	private static final String SERVER_ID		= "server_id";
	private static final String BUILD_ID		= "build_id";
	private static final String TIME			= "time";
	private static final String BUILD_STRING	= "build_string";
	private static final String TEST_STRING		= "test_string";
	private static final String COMPILE_TIME	= "compile_time";
	private static final String BUILD_STATE		= "build_state";
	private static final String TEST_FAILURE	= "test_failure";
	private static final String TEST_TOTAL		= "test_total";
	
	public ResponseBuildDetailedPacket(JSONObject json) {
		super(json, LightspeedPacketType.RESPONSE_BUILD_DETAILED);
	}
	
	public ResponseBuildDetailedPacket(SharedBuildData buildData) {
		super(LightspeedPacketType.RESPONSE_BUILD_DETAILED);
		setBuildData(buildData);
	}
	
	public String getServerId() {
		return getJSON().getString(SERVER_ID);
	}
	
	public long getBuildId() {
		return getJSON().getLong(BUILD_ID);
	}
	
	public long getTime() {
		return getJSON().getLong(TIME);
	}
	
	public String getBuildString() {
		return getJSON().getString(BUILD_STRING);
	}
	
	public String getTestString() {
		return getJSON().getString(TEST_STRING);
	}
	
	public double getCompileTime() {
		return getJSON().getDouble(COMPILE_TIME);
	}
	
	public BuildState getBuildState() {
		return BuildState.getState(getJSON().getString(BUILD_STATE));
	}
	
	public int getTestFailures() {
		return getJSON().getInt(TEST_FAILURE);
	}
	
	public int getTestTotal() {
		return getJSON().getInt(TEST_TOTAL);
	}
	
	public SharedBuildData getBuildData() {
		SharedBuildData build = new SharedBuildData(getJSON().getLong(BUILD_ID), new SharedServerData(getServerId()));
		build.setTime(getJSON().getLong(TIME));
		build.setBuildString(getBuildString());
		build.setTestString(getTestString());
		build.setCompileTime(getCompileTime());
		build.setBuildState(getBuildState());
		build.setTestDetails(new TestDetails(getTestTotal(), getTestFailures()));
		return build;
	}
	
	public void setServerId(String serverId) {
		getJSON().put(SERVER_ID, serverId);
	}
	
	public void setBuildId(long buildId) {
		getJSON().put(BUILD_ID, buildId);
	}
	
	public void setTime(long time) {
		getJSON().put(TIME, time);
	}
	
	public void setBuildString(String buildString) {
		getJSON().put(BUILD_STRING, buildString);
	}
	
	public void setTestString(String testString) {
		getJSON().put(TEST_STRING, testString);
	}
	
	public void setCompileTime(double compileTime) {
		getJSON().put(COMPILE_TIME, compileTime);
	}
	
	public void setBuildState(BuildState state) {
		getJSON().put(BUILD_STATE, state.name());
	}
	
	public void setTestDetails(TestDetails testDetails) {
		setTestDetails(testDetails.getTotal(), testDetails.getFailures());
	}
	
	public void setTestDetails(int total, int failures) {
		getJSON().put(TEST_TOTAL, total);
		getJSON().put(TEST_FAILURE, failures);
	}
	
	public void setBuildData(SharedBuildData buildData) {
		setServerId(buildData.getServer().getName());
		setBuildId(buildData.getId());
		setTime(buildData.getTime());
		setBuildString(buildData.getBuildString());
		setTestString(buildData.getTestString());
		setCompileTime(buildData.getCompileTime());
		setBuildState(buildData.getBuildState());
		setTestDetails(buildData.getTestDetails());
	}
	
}
