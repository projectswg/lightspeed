/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets;

import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.connection.AuthenticatePacket;
import com.projectswg.common.network.packets.post.*;
import com.projectswg.common.network.packets.request.*;
import com.projectswg.common.network.packets.response.*;
import me.joshlarson.json.JSONObject;

public class Packet {
	
	private static final String TYPE = "type";
	
	private final JSONObject json;
	private final LightspeedPacketType type;
	
	public Packet(JSONObject json, LightspeedPacketType type) {
		this.json = json;
		this.type = type;
		if (!json.containsKey(TYPE))
			throw new IllegalArgumentException("Not a valid packet JSON!");
		if (!json.getString(TYPE).equals(type.name()))
			throw new IllegalArgumentException("JSON type does not match packet type!");
	}
	
	public Packet(LightspeedPacketType type) {
		this.json = new JSONObject();
		this.type = type;
		getJSON().put(TYPE, type.name());
	}
	
	public JSONObject getJSON() {
		return json;
	}
	
	public LightspeedPacketType getType() {
		return type;
	}
	
	@Override
	public String toString() {
		return "LightspeedPacket["+type+"]";
	}
	
	public static JSONObject createPacketJson(LightspeedPacketType type) {
		JSONObject json = new JSONObject();
		json.put("type", type.name());
		return json;
	}
	
	public static Packet inflate(JSONObject json, LightspeedPacketType type) {
		switch (type) {
			case AUTHENTICATION:				return new AuthenticatePacket(json);
			case POST_BUILD:					return new PostBuildPacket(json);
			case POST_DEPLOY:					return new PostDeployPacket(json);
			case POST_STOP_BUILD:				return new PostStopBuildPacket(json);
			case POST_STOP_DEPLOY:				return new PostStopDeployPacket(json);
			case POST_RESULT:					return new PostResult(json);
			case REQUEST_SERVER_LIST:			return new RequestServerListPacket(json);
			case REQUEST_BUILD_LIST:			return new RequestBuildListPacket(json);
			case REQUEST_DEPLOYMENT_LIST:		return new RequestDeploymentListPacket(json);
			case REQUEST_SERVER_DETAILED:		return new RequestServerDetailedPacket(json);
			case REQUEST_BUILD_DETAILED:		return new RequestBuildDetailedPacket(json);
			case REQUEST_DEPLOYMENT_DETAILED:	return new RequestDeploymentDetailedPacket(json);
			case RESPONSE_SERVER_LIST:			return new ResponseServerListPacket(json);
			case RESPONSE_BUILD_LIST:			return new ResponseBuildListPacket(json);
			case RESPONSE_DEPLOYMENT_LIST:		return new ResponseDeploymentListPacket(json);
			case RESPONSE_SERVER_DETAILED:		return new ResponseServerDetailedPacket(json);
			case RESPONSE_BUILD_DETAILED:		return new ResponseBuildDetailedPacket(json);
			case RESPONSE_DEPLOYMENT_DETAILED:	return new ResponseDeploymentDetailedPacket(json);
		}
		Log.e("Packet", "Unable to inflate packet: %s", type);
		return null;
	}
	
}
