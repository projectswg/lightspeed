/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets.response;

import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.network.packets.LightspeedPacketType;
import me.joshlarson.json.JSONObject;

public class ResponseServerDetailedPacket extends ResponsePacket {
	
	private static final String SERVER_ID = "server_id";
	private static final String DIRECTORY = "directory";
	private static final String JVM_ARGS = "jvm_arguments";
	
	public ResponseServerDetailedPacket(JSONObject json) {
		super(json, LightspeedPacketType.RESPONSE_SERVER_DETAILED);
	}
	
	public ResponseServerDetailedPacket(SharedServerData serverData) {
		super(LightspeedPacketType.RESPONSE_SERVER_DETAILED);
		setServer(serverData);
	}
	
	public ResponseServerDetailedPacket(String serverId, String dir, String jvmArguments) {
		super(LightspeedPacketType.RESPONSE_SERVER_DETAILED);
		setServerId(serverId);
		setDirectory(dir);
		setJvmArguments(jvmArguments);
	}
	
	public String getServerId() {
		return getJSON().getString(SERVER_ID);
	}
	
	public String getDirectory() {
		return getJSON().getString(DIRECTORY);
	}
	
	public String getJvmArguments() {
		return getJSON().getString(JVM_ARGS);
	}
	
	public SharedServerData getServer() {
		SharedServerData server = new SharedServerData(getServerId());
		server.setDirectory(getDirectory());
		server.setJvmArguments(getJvmArguments());
		return server;
	}
	
	public void setServerId(String serverId) {
		getJSON().put(SERVER_ID, serverId);
	}
	
	public void setDirectory(String dir) {
		getJSON().put(DIRECTORY, dir);
	}
	
	public void setJvmArguments(String jvmArguments) {
		getJSON().put(JVM_ARGS, jvmArguments);
	}
	
	public void setServer(SharedServerData serverData) {
		setServerId(serverData.getName());
		setDirectory(serverData.getDirectory());
		setJvmArguments(serverData.getJvmArguments());
	}
	
}
