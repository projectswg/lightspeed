/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets.response;

import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.network.packets.LightspeedPacketType;
import me.joshlarson.json.JSONArray;
import me.joshlarson.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResponseServerListPacket extends ResponsePacket {
	
	private static final String SERVER_LIST = "server_list";
	
	public ResponseServerListPacket(JSONObject json) {
		super(json, LightspeedPacketType.RESPONSE_SERVER_LIST);
	}
	
	public ResponseServerListPacket(List<SharedServerData> serverList) {
		super(LightspeedPacketType.RESPONSE_SERVER_LIST);
		setServerList(serverList);
	}
	
	public List<SharedServerData> getServerList() {
		List<SharedServerData> serverList = new ArrayList<>();
		for (Object o : getJSON().getArray(SERVER_LIST)) {
			if (o instanceof String)
				serverList.add(new SharedServerData((String) o));
		}
		return serverList;
	}
	
	public void setServerList(List<SharedServerData> serverList) {
		JSONArray array = new JSONArray();
		for (SharedServerData server : serverList) {
			array.add(server.getName());
		}
		getJSON().put(SERVER_LIST, array);
	}
	
}
