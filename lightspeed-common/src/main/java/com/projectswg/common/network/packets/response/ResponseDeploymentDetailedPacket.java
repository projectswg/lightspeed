/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets.response;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedDeploymentData.DeploymentState;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.network.packets.LightspeedPacketType;
import me.joshlarson.json.JSONObject;

public class ResponseDeploymentDetailedPacket extends ResponsePacket {
	
	private static final String SERVER_ID		= "server_id";
	private static final String BUILD_ID		= "build_id";
	private static final String DEPLOYMENT_ID	= "deployment_id";
	private static final String START_TIME		= "start_time";
	private static final String END_TIME		= "end_time";
	private static final String OUTPUT			= "output";
	private static final String STATE			= "state";
	
	public ResponseDeploymentDetailedPacket(JSONObject json) {
		super(json, LightspeedPacketType.RESPONSE_DEPLOYMENT_DETAILED);
	}
	
	public ResponseDeploymentDetailedPacket(SharedDeploymentData deployment) {
		super(LightspeedPacketType.RESPONSE_DEPLOYMENT_DETAILED);
		setDeploymentData(deployment);
	}
	
	public String getServerId() {
		return getJSON().getString(SERVER_ID);
	}
	
	public long getBuildId() {
		return getJSON().getLong(BUILD_ID);
	}
	
	public long getDeploymentId() {
		return getJSON().getLong(DEPLOYMENT_ID);
	}
	
	public long getStartTime() {
		return getJSON().getLong(START_TIME);
	}
	
	public long getEndTime() {
		return getJSON().getLong(END_TIME);
	}
	
	public String getOutput() {
		return getJSON().getString(OUTPUT);
	}
	
	public DeploymentState getDeploymentState() {
		return DeploymentState.getState(getJSON().getString(STATE));
	}
	
	public SharedDeploymentData getDeploymentData() {
		SharedDeploymentData deploymentData = new SharedDeploymentData(getDeploymentId(), new SharedBuildData(getBuildId(), new SharedServerData(getServerId())));
		deploymentData.setStartTime(getStartTime());
		deploymentData.setEndTime(getEndTime());
		deploymentData.setOutput(getOutput());
		deploymentData.setDeploymentState(getDeploymentState());
		return deploymentData;
	}
	
	public void setServerId(String serverId) {
		getJSON().put(SERVER_ID, serverId);
	}
	
	public void setBuildId(long buildId) {
		getJSON().put(BUILD_ID, buildId);
	}
	
	public void setDeploymentId(long deploymentId) {
		getJSON().put(DEPLOYMENT_ID, deploymentId);
	}
	
	public void setStartTime(long startTime) {
		getJSON().put(START_TIME, startTime);
	}
	
	public void setEndTime(long endTime) {
		getJSON().put(END_TIME, endTime);
	}
	
	public void setOutput(String output) {
		getJSON().put(OUTPUT, output);
	}
	
	public void setDeploymentState(DeploymentState state) {
		getJSON().put(STATE, state.name());
	}
	
	public void setDeploymentData(SharedDeploymentData deploymentData) {
		setServerId(deploymentData.getServer().getName());
		setBuildId(deploymentData.getBuild().getId());
		setDeploymentId(deploymentData.getId());
		setStartTime(deploymentData.getStartTime());
		setEndTime(deploymentData.getEndTime());
		setOutput(deploymentData.getOutput());
		setDeploymentState(deploymentData.getDeploymentState());
	}
}
