/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.common.network.packets.response;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.network.packets.LightspeedPacketType;
import me.joshlarson.json.JSONArray;
import me.joshlarson.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ResponseDeploymentListPacket extends ResponsePacket {
	
	private static final String SERVER_ID = "server_id";
	private static final String BUILD_ID = "build_id";
	private static final String DEPLOYMENT_ID = "deployment_id";
	private static final String DEPLOYMENT_LIST = "deployment_list";
	
	public ResponseDeploymentListPacket(JSONObject json) {
		super(json, LightspeedPacketType.RESPONSE_DEPLOYMENT_LIST);
	}
	
	public ResponseDeploymentListPacket(String serverId, List<SharedDeploymentData> deploymentList) {
		super(LightspeedPacketType.RESPONSE_DEPLOYMENT_LIST);
		setServerId(serverId);
		setDeploymentList(deploymentList);
	}
	
	public String getServerId() {
		return getJSON().getString(SERVER_ID);
	}
	
	public List<SharedDeploymentData> getDeploymentList() {
		List<SharedDeploymentData> deploymentList = new ArrayList<>();
		SharedServerData server = new SharedServerData(getServerId());
		for (Object o : getJSON().getArray(DEPLOYMENT_LIST)) {
			if (o instanceof JSONObject) {
				JSONObject obj = (JSONObject) o;
				deploymentList.add(new SharedDeploymentData(obj.getLong(DEPLOYMENT_ID), new SharedBuildData(obj.getLong(BUILD_ID), server)));
			}
		}
		return deploymentList;
	}
	
	public void setServerId(String serverId) {
		getJSON().put(SERVER_ID, serverId);
	}
	
	public void setDeploymentList(List<SharedDeploymentData> deploymentList) {
		JSONArray array = new JSONArray();
		for (SharedDeploymentData deployment : deploymentList) {
			JSONObject obj = new JSONObject();
			obj.put(BUILD_ID, deployment.getBuild().getId());
			obj.put(DEPLOYMENT_ID, deployment.getId());
			array.add(obj);
		}
		getJSON().put(DEPLOYMENT_LIST, array);
	}
	
}
