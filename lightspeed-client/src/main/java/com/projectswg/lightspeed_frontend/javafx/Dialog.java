/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.javafx;

import javafx.geometry.Dimension2D;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.layout.Region;
import javafx.stage.Stage;

public class Dialog {
	
	private static final Parent DEFAULT_ROOT = new Region();
	
	private final Stage popup;
	
	private String title;
	private Parent root;
	private double width;
	private double height;
	private boolean resizable;
	
	public Dialog(String title) {
		this("", 300, 200);
	}
	
	public Dialog(String title, double width, double height) {
		this(title, width, height, null);
	}
	
	public Dialog(String title, double width, double height, Parent root) {
		if (root == null)
			root = DEFAULT_ROOT;
		this.popup = new Stage();
		this.title = title;
		this.width = width;
		this.height = height;
		this.root = root;
		
		this.resizable = false;
		this.popup.setScene(new Scene(root, width, height));
	}
	
	public Stage getStage() {
		return popup;
	}
	
	public String getTitle() {
		return title;
	}
	
	public Parent getRoot() {
		return root;
	}
	
	public double getWidth() {
		return width;
	}
	
	public double getHeight() {
		return height;
	}
	
	public Dimension2D getSize() {
		return new Dimension2D(width, height);
	}
	
	public boolean isResizable() {
		return resizable;
	}
	
	public void setTitle(String title) {
		this.title = title;
		popup.setTitle(title);
	}
	
	public void setRoot(Parent root) {
		this.root = root;
		popup.getScene().setRoot(root);
	}
	
	public void setWidth(double width) {
		this.width = width;
		popup.setWidth(width);
	}
	
	public void setHeight(double height) {
		this.height = height;
		popup.setHeight(height);
	}
	
	public void setSize(double width, double height) {
		setWidth(width);
		setHeight(height);
	}
	
	public void setResizable(boolean resizable) {
		popup.setResizable(resizable);
	}
	
	public Stage show() {
		if (root == null)
			throw new NullPointerException("Root cannot be null!");
		popup.centerOnScreen();
		popup.show();
		return popup;
	}
	
	public void hide() {
		if (popup != null)
			popup.hide();
	}
	
}
