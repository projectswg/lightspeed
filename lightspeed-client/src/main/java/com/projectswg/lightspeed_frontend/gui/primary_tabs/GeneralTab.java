/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui.primary_tabs;

import com.projectswg.common.concurrency.PswgScheduledThreadPool;
import com.projectswg.common.control.IntentManager;
import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.javafx.FXMLController;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.response.ResponseServerDetailedPacket;
import com.projectswg.common.utilities.TimeFormatUtilities;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed_frontend.Frontend;
import com.projectswg.lightspeed_frontend.LightspeedFrontendGUI;
import com.projectswg.lightspeed_frontend.intents.InboundDataUpdateIntent;
import javafx.application.Platform;
import javafx.beans.InvalidationListener;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.ResourceBundle;
import java.util.prefs.Preferences;

public class GeneralTab implements FXMLController {
	
	@FXML
	private Parent rootGeneral;
	@FXML
	private ComboBox<SharedServerData> serverComboBox;
	@FXML
	private Label serverNameText;
	@FXML
	private Label buildStatusText;
	@FXML
	private Label deploymentStatusText;
	@FXML
	private Label populationText;
	@FXML
	private Label uptimeText;
	
	private final PswgScheduledThreadPool scheduledUpdater;
	private final Frontend frontend;
	
	public GeneralTab() {
		this.scheduledUpdater = new PswgScheduledThreadPool(1, "general-tab-updater");
		this.frontend = LightspeedFrontendGUI.getFrontend();
	}
	
	@Override
	public Parent getRoot() {
		return rootGeneral;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FXMLUtilities.onFxmlLoaded(this);
		serverNameText.getStyleClass().setAll("white-label");
		buildStatusText.getStyleClass().setAll("white-label");
		deploymentStatusText.getStyleClass().setAll("white-label");
		populationText.getStyleClass().setAll("white-label");
		uptimeText.getStyleClass().setAll("white-label");
		serverComboBox.setConverter(new ServerStringConverter());
		scheduledUpdater.start();
		scheduledUpdater.executeWithFixedRate(1000, 1000, () -> Platform.runLater(this::updateGeneralDetails));
		
		updateGeneralDetails();
		serverComboBox.setItems(frontend.getData().getServers());
		frontend.getData().getServers().addListener((InvalidationListener) (l -> Platform.runLater(this::updateSelectedServer)));
		updateSelectedServer();
		serverComboBox.getSelectionModel().selectedItemProperty().addListener((val, prev, server) -> onSelectedServerUpdated(server));
		IntentManager.getInstance().registerForIntent(InboundDataUpdateIntent.class, this::handleInboundDataUpdateIntent);
	}
	
	@Override
	public void terminate() {
		scheduledUpdater.stop();
		scheduledUpdater.awaitTermination(1000);
	}
	
	private void handleInboundDataUpdateIntent(InboundDataUpdateIntent idui) {
		Packet packet = idui.getPacket();
		if (packet instanceof ResponseServerDetailedPacket) {
			Platform.runLater(this::updateGeneralDetails);
		}
	}
	
	private void onSelectedServerUpdated(SharedServerData server) {
		frontend.getData().setSelectedServer(server);
		if (server != null)
			getPreferences().put("selected-server", server.getName());
		else
			getPreferences().remove("selected-server");
		updateGeneralDetails();
	}
	
	private void updateSelectedServer() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		if (selectedServer != null)
			return;
		
		selectedServer = frontend.getData().getServer(getPreferences().get("selected-server", "NGE"));
		frontend.getData().setSelectedServer(selectedServer);
		serverComboBox.setValue(selectedServer);
	}
	
	private void updateGeneralDetails() {
		SharedServerData selected = frontend.getData().getSelectedServer();
		setServerName(selected);
		setBuildStatus(selected);
		setDeploymentStatus(selected);
		setPopulationText(selected);
		setUptimeText(selected);
	}
	
	private void setServerName(SharedServerData server) {
		String name = server == null ? "N/A" : server.getName();
		setText(serverNameText, TextStyle.WHITE, name);
	}
	
	private void setBuildStatus(SharedServerData server) {
		if (server == null) {
			setText(buildStatusText, TextStyle.WHITE, "N/A");
			return;
		}
		SharedBuildData lastBuild = server.getLastBuild();
		if (lastBuild == null) {
			setText(buildStatusText, TextStyle.WHITE, "Not Built");
			return;
		}
		BuildState state = lastBuild.getBuildState();
		TextStyle style = TextStyle.WHITE;
		switch (state) {
			case CREATED:
			case UNKNOWN:
				style = TextStyle.WHITE;
				break;
			case CANCELLED:
			case COMPILE_FAILED:
			case TEST_FAILED:
			case INSTALL_FAILED:
			case FAILED:
				style = TextStyle.RED;
				break;
			case COMPILING:
			case TESTING:
			case INSTALLING:
			case SUCCESS:
				style = TextStyle.GREEN;
				break;
		}
		setText(buildStatusText, style, state.toString());
	}
	
	private void setDeploymentStatus(SharedServerData server) {
		if (server == null) {
			setText(deploymentStatusText, TextStyle.WHITE, "N/A");
			return;
		}
		SharedDeploymentData deployment = server.getLastDeployment();
		if (deployment == null) {
			setText(deploymentStatusText, TextStyle.WHITE, "Not Deployed");
			return;
		}
		if (deployment.getEndTime() == 0) {
			setText(deploymentStatusText, TextStyle.GREEN, "Running");
		} else {
			setText(deploymentStatusText, TextStyle.WHITE, "Stopped");
		}
	}
	
	private void setPopulationText(SharedServerData server) {
		if (server == null) {
			setText(populationText, TextStyle.WHITE, "N/A");
			return;
		}
		SharedDeploymentData deployment = server.getLastDeployment();
		if (deployment == null) {
			setText(populationText, TextStyle.WHITE, "Not Deployed");
			return;
		}
		populationText.setText(String.format("%d Characters Online", 0));
	}
	
	private void setUptimeText(SharedServerData server) {
		if (server == null) {
			setText(uptimeText, TextStyle.WHITE, "N/A");
			return;
		}
		SharedDeploymentData deployment = server.getLastDeployment();
		if (deployment == null) {
			setText(uptimeText, TextStyle.WHITE, "Not Deployed");
			return;
		}
		long uptime;
		TextStyle style;
		if (deployment.getEndTime() == 0) {
			uptime = TimeUtilities.getTime() - deployment.getStartTime();
			style = TextStyle.GREEN;
		} else {
			uptime = deployment.getEndTime() - deployment.getStartTime();
			style = TextStyle.WHITE;
		}
		setText(uptimeText, style, TimeFormatUtilities.getTime(uptime - (uptime % 1000)));
	}
	
	private void setText(Label label, TextStyle style, String text) {
		switch (style) {
			case WHITE:	label.getStyleClass().setAll("white-label"); break;
			case GREEN:	label.getStyleClass().setAll("green-label"); break;
			case RED:	label.getStyleClass().setAll("red-label"); break;
		}
		label.setText(text);
	}
	
	private Preferences getPreferences() {
		return frontend.getPreferences().node("general_tab");
	}
	
	private enum TextStyle {
		WHITE,
		GREEN,
		RED
	}
	
	private class ServerStringConverter extends StringConverter<SharedServerData> {
		
		public String toString(SharedServerData server) {
			if (server == null)
				return "";
			return server.getName();
		}
		
		public SharedServerData fromString(String string) {
			if (string.isEmpty())
				return null;
			return frontend.getData().getServer(string);
		}
		
	}
	
}
