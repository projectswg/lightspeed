/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui;

import com.projectswg.common.debug.Log;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.*;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;

import javax.swing.*;
import java.awt.*;


public class InternalWindow extends Region {
	
	private final VBox windowPane;
	private final VBox contentPane;
	
	public InternalWindow(String title) {
		this(title, null);
	}
	
	public InternalWindow(String title, Node content) {
		windowPane = new VBox();
		contentPane = new VBox();
		windowPane.getChildren().addAll(createTitleBar(title), contentPane);
		windowPane.maxWidthProperty().bind(widthProperty());
		windowPane.prefHeightProperty().bind(heightProperty());
		VBox.setVgrow(contentPane, Priority.ALWAYS);
		getChildren().add(windowPane);
		String style = "";
		style += "-fx-background-color: white; ";
		style += "-fx-border-width: 1; -fx-border-color: black; ";
		style += "-fx-border-radius: 10 10 10 10; ";
		style += "-fx-background-radius: 10 10 0 0; ";
		setStyle(style);
		StackPane.setMargin(this, new Insets(20, 20, 20, 20));
		setContent(content);
	}
	
	public void setContent(Node content) {
		contentPane.getChildren().clear();
		if (content != null) {
			contentPane.getChildren().add(content);
			VBox.setVgrow(content, Priority.ALWAYS);
		}
	}
	
	private Node createTitleBar(String title) {
		AnchorPane titleBar = new AnchorPane();
		createTitleLabel(title, titleBar);
		createCloseButton(titleBar);
		String style = "-fx-padding: 3; ";
		style += "-fx-background-color: "+getHex(UIManager.getColor("Panel.background"))+"; ";
		style += "-fx-border-width: 1; -fx-border-color: black; ";
		style += "-fx-border-radius: 10 10 0 0; ";
		style += "-fx-background-radius: 10 0 0 0; ";
		titleBar.setStyle(style);
		titleBar.setCache(true);
		titleBar.minWidthProperty().bind(widthProperty());
		titleBar.prefWidthProperty().bind(widthProperty());
		titleBar.maxWidthProperty().bind(widthProperty());
		titleBar.setPrefHeight(30);
		titleBar.setMaxHeight(30);
		VBox.setVgrow(titleBar, Priority.NEVER);
		return titleBar;
	}
	
	private void createTitleLabel(String title, AnchorPane titleBar) {
		Label titleLabel = new Label(title);
		titleLabel.setFont(Font.font(titleLabel.getFont().getFamily(), FontWeight.BOLD, titleLabel.getFont().getSize()));
		HBox titleWrapper = new HBox(titleLabel);
		titleWrapper.setAlignment(Pos.CENTER);
		AnchorPane.setTopAnchor(titleWrapper, 0.0);
		titleWrapper.prefWidthProperty().bind(titleBar.widthProperty());
		titleWrapper.prefHeightProperty().bind(titleBar.heightProperty());
		titleWrapper.setMaxHeight(30);
		titleBar.getChildren().add(titleWrapper);
	}
	
	private void createCloseButton(AnchorPane titleBar) {
		Button closeButton = new Button("x");
		closeButton.setMaxSize(30, 30);
		VBox closeWrapper = new VBox(closeButton);
		closeWrapper.setAlignment(Pos.CENTER_RIGHT);
		closeWrapper.setPadding(new Insets(5, 0, 0, 0));
		AnchorPane.setRightAnchor(closeWrapper, 0.0);
		closeButton.setOnAction((e) -> removeFromParent());
		closeWrapper.prefHeightProperty().bind(titleBar.heightProperty());
		closeWrapper.setMaxHeight(30);
		titleBar.getChildren().add(closeWrapper);
	}
	
	private String getHex(Color c) {
		return String.format("#%02X%02X%02X", c.getRed(), c.getGreen(), c.getBlue());
	}
	
	private void removeFromParent() {
		Node parent = getParent();
		if (parent instanceof Pane) {
			((Pane) parent).getChildren().remove(this);
			parent.requestFocus();
		} else {
			Log.e("Unable to remove InternalWindow from parent: %s", parent);
		}
	}
	
}
