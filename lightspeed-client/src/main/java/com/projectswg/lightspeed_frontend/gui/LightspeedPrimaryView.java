/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui;

import com.projectswg.common.concurrency.PswgBasicScheduledThread;
import com.projectswg.common.concurrency.PswgThreadPool;
import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.javafx.FXMLController;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.common.network.packets.post.PostBuildPacket;
import com.projectswg.common.network.packets.post.PostDeployPacket;
import com.projectswg.common.network.packets.post.PostStopDeployPacket;
import com.projectswg.lightspeed_frontend.Frontend;
import com.projectswg.lightspeed_frontend.LightspeedFrontendGUI;
import javafx.application.Platform;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.MenuButton;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.layout.Background;
import javafx.scene.layout.BackgroundFill;
import javafx.scene.layout.Region;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.ResourceBundle;

public class LightspeedPrimaryView implements FXMLController {
	
	@FXML
	private VBox primaryViewContainer;
	@FXML
	private TextField ipAddressText, portText;
	@FXML
	private Region validIpRegion;
	@FXML
	private MenuButton commonServerStatus;
	@FXML
	private MenuButton commonBuildStatus;
	@FXML
	private MenuButton commonDeploymentStatus;
	
	private final PswgBasicScheduledThread scheduledUpdater;
	private final PswgThreadPool ipUpdater;
	private final Frontend frontend;
	private final Background validIpBackground;
	private final Background invalidIpBackground;
	
	public LightspeedPrimaryView() {
		this.scheduledUpdater = new PswgBasicScheduledThread("primary-view-updater", this::updateUi);
		this.ipUpdater = new PswgThreadPool(1, "primary-view-ip-updater");
		this.frontend = LightspeedFrontendGUI.getFrontend();
		this.validIpBackground = new Background(new BackgroundFill(Color.GREEN, null, null));
		this.invalidIpBackground = new Background(new BackgroundFill(Color.RED, null, null));
	}
	
	@Override
	public Parent getRoot() {
		return primaryViewContainer;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FXMLUtilities.onFxmlLoaded(this);
		ipUpdater.start();
		ipAddressText.setText(frontend.getCommunication().getAddress().getHostString());
		portText.setText(Integer.toString(frontend.getCommunication().getAddress().getPort()));
		validIpRegion.setBackground(validIpBackground);
		updateAddress();
		ipAddressText.focusedProperty().addListener(listener -> updateAddress());
		ipAddressText.setOnKeyPressed(ke -> {if (ke.getCode() == KeyCode.ENTER) {updateAddress();}});
		portText.setOnKeyPressed(ke -> {if (ke.getCode() == KeyCode.ENTER) {updateAddress();}});
		portText.focusedProperty().addListener(listener -> updateAddress());
		frontend.getData().getSelectedServerProperty().addListener((val, prev, next) -> updateUi());
		updateUi();
		scheduledUpdater.startWithFixedRate(1000, 1000);
		updateAddress();
	}
	
	@Override
	public void terminate() {
		scheduledUpdater.stop();
		ipUpdater.stop(false);
		scheduledUpdater.awaitTermination(1000);
		ipUpdater.awaitTermination(1000);
	}
	
	private void updateAddress() {
		ipUpdater.execute(() -> {
			InetSocketAddress addr = createAddress();
			frontend.setAddress(addr);
			Platform.runLater(() -> validIpRegion.setBackground(addr == null ? invalidIpBackground : validIpBackground));
		});
	}
	
	private InetSocketAddress createAddress() {
		String hostText = this.ipAddressText.getText();
		String portText = this.portText.getText();
		if (hostText.isEmpty() || portText.isEmpty())
			return null;
		try {
			InetAddress addr = InetAddress.getByName(hostText);
			int port = Integer.parseInt(portText);
			return new InetSocketAddress(addr, port);
		} catch (NumberFormatException | UnknownHostException e) {
			return null;
		}
	}
	
	private void updateUi() {
		Platform.runLater(() -> {
			setBuildStatusText();
			setDeploymentStatusText();
		});
	}
	
	private void setBuildStatusText() {
		SharedServerData server = frontend.getData().getSelectedServer();
		SharedBuildData build = server == null ? null : server.getLastBuild();
		if (build != null) {
			commonBuildStatus.setText(String.format("Build: %s", build.getBuildState()));
		} else {
			commonBuildStatus.setText("Build: N/A");
		}
		if (server != null && commonBuildStatus.getItems().isEmpty()) {
			MenuItem startBuild = new MenuItem("Start Build");
			startBuild.setOnAction((event) -> frontend.send(new PostBuildPacket(server.getName())));
			commonBuildStatus.getItems().setAll(startBuild);
		} else {
			if (server == null)
				commonBuildStatus.getItems().clear();
		}
	}
	
	private void setDeploymentStatusText() {
		SharedServerData server = frontend.getData().getSelectedServer();
		SharedDeploymentData deployment = server == null ? null : server.getLastDeployment();
		if (deployment != null) {
			commonDeploymentStatus.setText("Deployment: " + deployment.getDeploymentState());
		} else {
			commonDeploymentStatus.setText("Deployment: N/A");
		}
		if (server != null && commonDeploymentStatus.getItems().isEmpty()) {
			MenuItem startDeploy = new MenuItem("Start Deployment");
			MenuItem stopDeploy = new MenuItem("Stop Deployment");
			startDeploy.setOnAction((event) -> frontend.send(new PostDeployPacket(server.getName())));
			stopDeploy.setOnAction((event) -> frontend.send(new PostStopDeployPacket(server.getName())));
			commonDeploymentStatus.getItems().setAll(startDeploy, stopDeploy);
		} else {
			if (server == null)
				commonDeploymentStatus.getItems().clear();
		}
	}
	
}
