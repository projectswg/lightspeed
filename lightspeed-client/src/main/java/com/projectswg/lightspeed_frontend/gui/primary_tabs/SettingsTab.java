/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui.primary_tabs;

import com.projectswg.common.javafx.FXMLController;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.lightspeed_frontend.Frontend;
import com.projectswg.lightspeed_frontend.LightspeedFrontendGUI;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.stage.FileChooser;

import java.io.File;
import java.net.URL;
import java.util.ResourceBundle;

public class SettingsTab implements FXMLController {
	
	@FXML
	private Parent rootSettings;
	@FXML
	private CheckBox securityCheckBox;
	@FXML
	private Button keystoreButton;
	@FXML
	private PasswordField keystorePasswordTextField;
	@FXML
	private TextField usernameTextField;
	@FXML
	private PasswordField passwordTextField;
	
	private final Frontend frontend;
	
	public SettingsTab() {
		this.frontend = LightspeedFrontendGUI.getFrontend();
	}
	
	@Override
	public Parent getRoot() {
		return rootSettings;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FXMLUtilities.onFxmlLoaded(this);
		keystoreButton.setOnAction(e -> setKeystorePathPopup());
		securityCheckBox.selectedProperty().bindBidirectional(frontend.getData().getSecurityProperty());
		keystoreButton.textProperty().bindBidirectional(frontend.getData().getKeystorePathProperty());
		keystorePasswordTextField.textProperty().bindBidirectional(frontend.getData().getKeystorePasswordProperty());
		usernameTextField.textProperty().bindBidirectional(frontend.getData().getUsernameProperty());
		passwordTextField.textProperty().bindBidirectional(frontend.getData().getPasswordProperty());
	}
	
	private void setKeystorePathPopup() {
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Set the keystore path");
		File file = fileChooser.showOpenDialog(frontend.getPrimaryStage());
		if (file == null || !file.isFile())
			return;
		keystoreButton.setText(file.getAbsolutePath());
	}
	
}
