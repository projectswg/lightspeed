/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui.primary_tabs;

import com.projectswg.common.concurrency.PswgScheduledThreadPool;
import com.projectswg.common.control.IntentManager;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.Log.LogLevel;
import com.projectswg.common.javafx.FXMLController;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.response.ResponseDeploymentDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseDeploymentListPacket;
import com.projectswg.common.utilities.TimeFormatUtilities;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed_frontend.Frontend;
import com.projectswg.lightspeed_frontend.LightspeedFrontendGUI;
import com.projectswg.lightspeed_frontend.intents.InboundPacketIntent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextArea;
import javafx.stage.FileChooser;
import javafx.util.StringConverter;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.charset.StandardCharsets;
import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

public class DeploymentTab implements FXMLController {
	
	private static final SharedDeploymentData NULL_DEPLOY = new SharedDeploymentData(Long.MAX_VALUE, null);
	private static final Pattern LOG_FILTER_PATTERN = Pattern.compile("^[^A-Z]*([A-Z]{1})[^\r\n]*$", Pattern.MULTILINE);
	
	@FXML
	private Parent rootDeployment;
	@FXML
	private ComboBox<SharedDeploymentData> deploymentComboBox;
	@FXML
	private ComboBox<LogLevel> logVerbosityComboBox;
	@FXML
	private ComboBox<Integer> logSizeComboBox;
	@FXML
	private Label serverNameText;
	@FXML
	private Label buildIdText;
	@FXML
	private Label deploymentIdText;
	@FXML
	private Label statusText;
	@FXML
	private Label startTimeText;
	@FXML
	private Label uptimeText;
	@FXML
	private Button logDownloadButton;
	@FXML
	private TextArea logTextArea;
	
	private final PswgScheduledThreadPool scheduledUpdater;
	private final ObservableList<SharedDeploymentData> deploymentList;
	private final Frontend frontend;
	
	public DeploymentTab() {
		this.scheduledUpdater = new PswgScheduledThreadPool(1, "deployment-tab-updater");
		this.deploymentList = FXCollections.observableArrayList();
		this.frontend = LightspeedFrontendGUI.getFrontend();
	}
	
	@Override
	public Parent getRoot() {
		return rootDeployment;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FXMLUtilities.onFxmlLoaded(this);
		serverNameText.getStyleClass().setAll("white-label");
		buildIdText.getStyleClass().setAll("white-label");
		deploymentIdText.getStyleClass().setAll("white-label");
		statusText.getStyleClass().setAll("white-label");
		startTimeText.getStyleClass().setAll("white-label");
		uptimeText.getStyleClass().setAll("white-label");
		deploymentComboBox.setConverter(new DeploymentStringConverter());
		deploymentComboBox.setItems(deploymentList);
		logDownloadButton.setOnAction(eh -> downloadLog());
		logVerbosityComboBox.setItems(FXCollections.observableArrayList(LogLevel.values()));
		logVerbosityComboBox.setValue(LogLevel.VERBOSE);
		logVerbosityComboBox.valueProperty().addListener((obs, prev, val) -> updateDeploymentDetails());
		logSizeComboBox.setItems(FXCollections.observableArrayList(50, 100, 200, 500, Integer.MAX_VALUE));
		logSizeComboBox.setConverter(new LogStringConverter());
		logSizeComboBox.setValue(200);
		logSizeComboBox.valueProperty().addListener((obs, prev, val) -> updateDeploymentDetails());
		scheduledUpdater.start();
		scheduledUpdater.executeWithFixedRate(1000, 1000, () -> Platform.runLater(this::updateDeploymentDetails));
		
		updateDeploymentList();
		updateDeploymentDetails();
		deploymentComboBox.getSelectionModel().selectedItemProperty().addListener((val, prev, next) -> updateDeploymentDetails());
		frontend.getData().getSelectedServerProperty().addListener((val, prev, next) -> Platform.runLater(this::updateDeploymentList));
		IntentManager.getInstance().registerForIntent(InboundPacketIntent.class, this::handleInboundPacketIntent);
	}
	
	@Override
	public void terminate() {
		scheduledUpdater.stop();
		scheduledUpdater.awaitTermination(1000);
	}
	
	private void handleInboundPacketIntent(InboundPacketIntent ipi) {
		Packet packet = ipi.getPacket();
		if (packet instanceof ResponseDeploymentListPacket) {
			Platform.runLater(this::updateDeploymentList);
		} else if (packet instanceof ResponseDeploymentDetailedPacket) {
			Platform.runLater(this::updateDeploymentDetails);
		}
	}
	
	private int sortDeployments(SharedDeploymentData a, SharedDeploymentData b) {
		if (a == null)
			return -1;
		if (b == null)
			return 1;
		return Long.compare(b.getId(), a.getId());
	}
	
	private void updateDeploymentList() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		if (selectedServer == null) {
			updateDeploymentDetails();
			deploymentList.clear();
			return;
		}
		
		List<SharedDeploymentData> updated = selectedServer.getDeployments().stream().sorted(this::sortDeployments).collect(Collectors.toList());
		updated.add(0, NULL_DEPLOY);
		if (updated.equals(deploymentList))
			return;
		
		updateDeploymentDetails();
		deploymentList.setAll(updated);
	}
	
	private void updateDeploymentDetails() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		SharedDeploymentData selectedDeployment;
		if (selectedServer == null) {
			selectedDeployment = null;
		} else {
			selectedDeployment = deploymentComboBox.getValue();
			if (selectedDeployment == null) {
				Platform.runLater(() -> deploymentComboBox.setValue(NULL_DEPLOY));
				return;
			}
			if (selectedDeployment == NULL_DEPLOY) {
				Optional<SharedDeploymentData> opt = selectedServer.getDeployments().stream().sorted(this::sortDeployments).findFirst();
				selectedDeployment = opt.orElse(null);
			}
		}
		
		setServerName(selectedServer);
		setBuildId(selectedDeployment);
		setDeploymentId(selectedDeployment);
		setStatus(selectedDeployment);
		setStartTime(selectedDeployment);
		setUptime(selectedDeployment);
		setLog(selectedDeployment);
	}
	
	private void setServerName(SharedServerData selectedServer) {
		if (selectedServer == null) {
			serverNameText.setText("N/A");
		} else {
			serverNameText.setText(selectedServer.getName());
		}
	}
	
	private void setBuildId(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null) {
			buildIdText.setText("N/A");
		} else {
			buildIdText.setText(Long.toString(selectedDeployment.getBuild().getId()));
		}
	}
	
	private void setDeploymentId(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null) {
			deploymentIdText.setText("N/A");
		} else {
			deploymentIdText.setText(Long.toString(selectedDeployment.getId()));
		}
	}
	
	private void setStatus(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null || selectedDeployment.getStartTime() == 0) {
			statusText.getStyleClass().setAll("white-label");
			statusText.setText("N/A");
		} else {
			if (selectedDeployment.getEndTime() == 0) {
				statusText.getStyleClass().setAll("green-label");
				String status = selectedDeployment.getDeploymentState().name().toLowerCase(Locale.US);
				status = Character.toUpperCase(status.charAt(0)) + status.substring(1);
				status = status.replace('_', ' ');
				statusText.setText(status);
			} else {
				statusText.getStyleClass().setAll("white-label");
				statusText.setText("Stopped at " + TimeUtilities.getDateString(TimeUtilities.convertUtcToLocal(selectedDeployment.getEndTime())));
			}
		}
	}
	
	private void setStartTime(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null || selectedDeployment.getStartTime() == 0) {
			startTimeText.setText("N/A");
		} else {
			startTimeText.setText(TimeUtilities.getDateString(TimeUtilities.convertUtcToLocal(selectedDeployment.getStartTime())));
		}
	}
	
	private void setUptime(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null || selectedDeployment.getStartTime() == 0) {
			uptimeText.setText("N/A");
		} else {
			long uptime;
			if (selectedDeployment.getEndTime() == 0) {
				uptime = TimeUtilities.getTime()-selectedDeployment.getStartTime();
			} else {
				uptime = selectedDeployment.getEndTime()-selectedDeployment.getStartTime();
			}
			uptimeText.setText(TimeFormatUtilities.getTime(uptime - (uptime % 1000)));
		}
	}
	
	private void setLog(SharedDeploymentData selectedDeployment) {
		if (selectedDeployment == null || selectedDeployment.getStartTime() == 0) {
			logTextArea.setText("");
		} else {
			String output = tail(filterLog(logVerbosityComboBox.getValue(), selectedDeployment.getOutput()), logSizeComboBox.getValue());
			String oldOutput = logTextArea.getText();
			if (!output.equals(oldOutput)) {
				double pos = logTextArea.getScrollTop();
				int anchor = logTextArea.getAnchor();
				int caret = logTextArea.getCaretPosition();
				logTextArea.setText(output);
				if (anchor != caret) {
					logTextArea.setScrollTop(pos);
					logTextArea.selectRange(anchor, caret);
				} else {
					logTextArea.setScrollTop(Double.MAX_VALUE);
				}
			}
		}
	}
	
	private String filterLog(LogLevel level, String log) {
		String line;
		Matcher m = LOG_FILTER_PATTERN.matcher(log);
		StringBuilder filtered = new StringBuilder();
		LogLevel lineLevel;
		String lineLevelStr;
		while (m.find()) {
			line = m.group(0);
			lineLevelStr = m.group(1);
			if (lineLevelStr.isEmpty()) {
				Log.w("Invalid line [EMPTYGROUP]: %s", line);
				continue;
			}
			lineLevel = getLogLevelFromCharacter(lineLevelStr.charAt(0));
			if (lineLevel.compareTo(level) >= 0) {
				filtered.append(line);
				filtered.append('\n');
			}
		}
		return filtered.toString();
	}
	
	private LogLevel getLogLevelFromCharacter(char c) {
		switch (c) {
			default:
			case 'V':
				return LogLevel.VERBOSE;
			case 'D':
				return LogLevel.DEBUG;
			case 'I':
				return LogLevel.INFO;
			case 'W':
				return LogLevel.WARN;
			case 'E':
				return LogLevel.ERROR;
			case 'A':
				return LogLevel.ASSERT;
		}
	}
	
	private void downloadLog() {
		SharedDeploymentData selectedDeployment = getSelectedDeployment();
		if (selectedDeployment == null || selectedDeployment.getStartTime() == 0) {
			// TODO: Simple little popup saying cannot download
		} else {
			File downloadFile = createDownloadLogFile(selectedDeployment);
			if (downloadFile == null) {
				Log.i("Download log aborted");
				return;
			}
			try (FileOutputStream fos = new FileOutputStream(downloadFile)) {
				fos.write(selectedDeployment.getOutput().getBytes(StandardCharsets.UTF_8));
			} catch (IOException e) {
				Log.e(e);
			}
		}
	}
	
	private File createDownloadLogFile(SharedDeploymentData selectedDeployment) {
		String time = new SimpleDateFormat("MMdd-HHmmss").format(System.currentTimeMillis());
		FileChooser fileChooser = new FileChooser();
		fileChooser.setTitle("Save download log");
		fileChooser.setInitialFileName(String.format("deployment-log-ID%d-TIME%s.log", selectedDeployment.getId(), time));
		File file = fileChooser.showSaveDialog(frontend.getPrimaryStage());
		if (file == null)
			return null;
		return file;
	}
	
	private SharedDeploymentData getSelectedDeployment() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		SharedDeploymentData selectedDeployment;
		if (selectedServer == null) {
			return null;
		} else {
			selectedDeployment = deploymentComboBox.getValue();
			if (selectedDeployment == NULL_DEPLOY) {
				Optional<SharedDeploymentData> opt = selectedServer.getDeployments().stream().sorted(this::sortDeployments).findFirst();
				return opt.orElse(null);
			}
			return selectedDeployment;
		}
	}
	
	private static String tail(String str, int n) {
		int index = str.length();
		for (int i = 0; i < n && index > 0; i++) {
			index = str.lastIndexOf('\n', index-1);
		}
		if (index <= 0)
			return str;
		return str.substring(index);
	}
	
	private class DeploymentStringConverter extends StringConverter<SharedDeploymentData> {
		
		@Override
		public String toString(SharedDeploymentData deployment) {
			if (deployment == NULL_DEPLOY || deployment == null)
				return "latest";
			return "D " + deployment.getId() + " - B " + deployment.getBuild().getId();
		}
		
		@Override
		public SharedDeploymentData fromString(String string) {
			if (string.isEmpty() || string.equals("latest"))
				return NULL_DEPLOY;
			String [] parts = string.split(" ");
			return frontend.getData().getDeployment(Long.parseLong(parts[1]));
		}
		
	}
	
	private class LogStringConverter extends StringConverter<Integer> {
		
		@Override
		public String toString(Integer i) {
			if (i == Integer.MAX_VALUE)
				return "All";
			return i.toString();
		}
		
		@Override
		public Integer fromString(String string) {
			if (string.isEmpty() || string.equals("All"))
				return Integer.MAX_VALUE;
			return Integer.parseInt(string);
		}
		
	}
	
}
