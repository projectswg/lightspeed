/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.gui.primary_tabs;

import com.projectswg.common.control.IntentManager;
import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedBuildData.BuildState;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.javafx.FXMLController;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.response.ResponseBuildDetailedPacket;
import com.projectswg.common.network.packets.response.ResponseBuildListPacket;
import com.projectswg.common.utilities.TimeFormatUtilities;
import com.projectswg.common.utilities.TimeUtilities;
import com.projectswg.lightspeed_frontend.Frontend;
import com.projectswg.lightspeed_frontend.LightspeedFrontendGUI;
import com.projectswg.lightspeed_frontend.intents.InboundDataUpdateIntent;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.fxml.FXML;
import javafx.scene.Parent;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.util.StringConverter;

import java.net.URL;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

public class BuildTab implements FXMLController {
	
	private static final SharedBuildData NULL_BUILD = new SharedBuildData(Long.MAX_VALUE, null);
	
	@FXML
	private Parent rootBuild;
	@FXML
	private ComboBox<SharedBuildData> buildComboBox;
	@FXML
	private Label serverNameText, buildIdText, statusText, timeText, compileTimeText, testText;
	
	private final Frontend frontend;
	private final ObservableList<SharedBuildData> buildList;
	
	public BuildTab() {
		this.frontend = LightspeedFrontendGUI.getFrontend();
		this.buildList = FXCollections.observableArrayList();
	}
	
	@Override
	public Parent getRoot() {
		return rootBuild;
	}
	
	@Override
	public void initialize(URL location, ResourceBundle resources) {
		FXMLUtilities.onFxmlLoaded(this);
		serverNameText.getStyleClass().setAll("white-label");
		buildIdText.getStyleClass().setAll("white-label");
		statusText.getStyleClass().setAll("white-label");
		timeText.getStyleClass().setAll("white-label");
		compileTimeText.getStyleClass().setAll("white-label");
		testText.getStyleClass().setAll("white-label");
		buildComboBox.setConverter(new BuildStringConverter());
		buildComboBox.setItems(buildList);
		
		updateBuildList();
		updateBuildDetails();
		buildComboBox.getSelectionModel().selectedItemProperty().addListener((val, prev, next) -> updateBuildDetails());
		frontend.getData().getSelectedServerProperty().addListener((val, prev, next) -> Platform.runLater(this::updateBuildList));
		IntentManager.getInstance().registerForIntent(InboundDataUpdateIntent.class, this::handleInboundDataUpdateIntent);
	}
	
	private void handleInboundDataUpdateIntent(InboundDataUpdateIntent idui) {
		Packet packet = idui.getPacket();
		if (packet instanceof ResponseBuildListPacket) {
			Platform.runLater(this::updateBuildList);
		} else if (packet instanceof ResponseBuildDetailedPacket) {
			Platform.runLater(this::updateBuildDetails);
		}
	}
	
	private int sortBuilds(SharedBuildData a, SharedBuildData b) {
		if (a == null || a == NULL_BUILD)
			return -1;
		if (b == null || b == NULL_BUILD)
			return 1;
		return Long.compare(b.getId(), a.getId());
	}
	
	private void updateBuildList() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		if (selectedServer == null) {
			updateBuildDetails();
			buildList.clear();
			return;
		}
		
		List<SharedBuildData> updated = selectedServer.getBuilds().stream().sorted(this::sortBuilds).collect(Collectors.toList());
		updated.add(0, NULL_BUILD);
		if (updated.equals(buildList))
			return;
		
		updateBuildDetails();
		buildList.setAll(updated);
	}
	
	private void updateBuildDetails() {
		SharedServerData selectedServer = frontend.getData().getSelectedServer();
		SharedBuildData selectedBuild;
		if (selectedServer == null) {
			selectedBuild = null;
		} else {
			selectedBuild = buildComboBox.getValue();
			if (selectedBuild == null) {
				Platform.runLater(() -> buildComboBox.setValue(NULL_BUILD));
				return;
			}
			if (selectedBuild == NULL_BUILD) {
				Optional<SharedBuildData> opt = selectedServer.getBuilds().stream().sorted(this::sortBuilds).findFirst();
				selectedBuild = opt.orElse(null);
			}
		}
		
		setServerName(selectedServer);
		setBuildId(selectedBuild);
		setStatus(selectedBuild);
		setTime(selectedBuild);
		setCompileTime(selectedBuild);
		setTests(selectedBuild);
	}
	
	private void setServerName(SharedServerData selectedServer) {
		if (selectedServer == null) {
			serverNameText.setText("N/A");
		} else {
			serverNameText.setText(selectedServer.getName());
		}
	}
	
	private void setBuildId(SharedBuildData selectedBuild) {
		if (selectedBuild == null) {
			buildIdText.setText("N/A");
		} else {
			buildIdText.setText(Long.toString(selectedBuild.getId()));
		}
	}
	
	private void setStatus(SharedBuildData selectedBuild) {
		if (selectedBuild == null) {
			statusText.getStyleClass().setAll("white-label");
			statusText.setText("N/A");
		} else {
			BuildState state = selectedBuild.getBuildState();
			switch (state) {
				case CREATED:
				case UNKNOWN:
					statusText.getStyleClass().setAll("white-label");
					break;
				case CANCELLED:
				case COMPILE_FAILED:
				case TEST_FAILED:
				case INSTALL_FAILED:
				case FAILED:
					statusText.getStyleClass().setAll("red-label");
					break;
				case COMPILING:
				case TESTING:
				case INSTALLING:
				case SUCCESS:
					statusText.getStyleClass().setAll("green-label");
					break;
			}
			statusText.setText(state.toString());
		}
	}
	
	private void setTime(SharedBuildData selectedBuild) {
		if (selectedBuild == null) {
			timeText.setText("N/A");
		} else {
			timeText.setText(TimeUtilities.getDateString(TimeUtilities.convertUtcToLocal(selectedBuild.getTime())));
		}
	}
	
	private void setCompileTime(SharedBuildData selectedBuild) {
		if (selectedBuild == null) {
			compileTimeText.setText("N/A");
		} else {
			compileTimeText.setText(TimeFormatUtilities.getTime((long) selectedBuild.getCompileTime()));
		}
	}
	
	private void setTests(SharedBuildData selectedBuild) {
		if (selectedBuild == null) {
			testText.setText("N/A");
		} else {
			TestDetails details = selectedBuild.getTestDetails();
			int total = details.getTotal();
			int passed = total-details.getFailures();
			testText.setText(String.format("%d of %d passed", passed, total));
		}
	}
	
	private class BuildStringConverter extends StringConverter<SharedBuildData> {
		
		public String toString(SharedBuildData build) {
			if (build == NULL_BUILD || build == null)
				return "latest";
			return "B " + build.getId();
		}
		
		public SharedBuildData fromString(String string) {
			if (string.isEmpty() || string.equals("latest"))
				return NULL_BUILD;
			String [] parts = string.split(" ");
			return frontend.getData().getBuild(Long.parseLong(parts[1]));
		}
		
	}
	
}
