/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend;

import com.projectswg.common.concurrency.PswgScheduledThreadPool;
import com.projectswg.common.concurrency.PswgThreadPool;
import com.projectswg.common.control.IntentManager;
import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.data.TestDetails;
import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.request.*;
import com.projectswg.common.network.packets.response.*;
import com.projectswg.lightspeed_frontend.communication.FrontendCommunication;
import com.projectswg.lightspeed_frontend.intents.InboundConnectionUpdateIntent;
import com.projectswg.lightspeed_frontend.intents.InboundDataUpdateIntent;
import com.projectswg.lightspeed_frontend.intents.InboundPacketIntent;
import javafx.stage.Stage;

import java.net.InetSocketAddress;
import java.util.List;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;
import java.util.prefs.Preferences;

public class Frontend {
	
	private final PswgThreadPool threadPool;
	private final PswgScheduledThreadPool scheduledUpdater;
	private final AtomicReference<Stage> primaryStage;
	private final AtomicBoolean connected;
	private final AtomicBoolean canSendRequestLatestBuild;
	private final AtomicBoolean canSendRequestLatestDeployment;
	private final FrontendCommunication communication;
	private final FrontendData data;
	private final Preferences preferences;
	
	public Frontend() {
		this.threadPool = new PswgThreadPool(5, "frontend-thread-pool");
		this.scheduledUpdater = new PswgScheduledThreadPool(1, "frontend-updater");
		this.primaryStage = new AtomicReference<>(null);
		this.connected = new AtomicBoolean(false);
		this.canSendRequestLatestBuild = new AtomicBoolean(true);
		this.canSendRequestLatestDeployment = new AtomicBoolean(true);
		this.preferences = Preferences.userNodeForPackage(getClass());
		
		this.communication = new FrontendCommunication(this);
		this.data = new FrontendData(this);
	}
	
	public void setAddress(InetSocketAddress address) {
		Log.i("Changing remote address to %s", address);
		if ((address == null && communication.getAddress() != null) || (address != null && !address.equals(communication.getAddress()))) {
			communication.setAddress(address);
		}
	}
	
	public void setPrimaryStage(Stage stage) {
		primaryStage.set(stage);
	}
	
	public Stage getPrimaryStage() {
		return primaryStage.get();
	}
	
	public void start() {
		IntentManager.getInstance().registerForIntent(InboundPacketIntent.class, this::handleInboundPacketIntent);
		IntentManager.getInstance().registerForIntent(InboundConnectionUpdateIntent.class, this::handleInboundConnectionUpdateIntent);
		data.setupSecurity();
		Assert.test(communication.start());
		threadPool.start();
		scheduledUpdater.start();
		scheduledUpdater.executeWithFixedDelay(0, 10000, this::requestCompleteLoad);
		scheduledUpdater.executeWithFixedDelay(5000, 1000, this::requestLatestUpdates);
		scheduledUpdater.executeWithFixedDelay(5000, 5000, this::probeConnectivity);
	}
	
	public void stop() {
		Assert.test(communication.stop());
		threadPool.stop(true);
		scheduledUpdater.stop();
		threadPool.awaitTermination(1000);
	}
	
	public FrontendCommunication getCommunication() {
		return communication;
	}
	
	public FrontendData getData() {
		return data;
	}
	
	public Preferences getPreferences() {
		return preferences;
	}
	
	public void probeConnectivity() {
		threadPool.execute(communication::probeConnectivity);
	}
	
	public void send(Packet p) {
		communication.send(p);
	}
	
	private void loadServerDetailed(String serverId) {
		send(new RequestServerDetailedPacket(serverId));
	}
	
	public void loadBuildDetailed(long buildId) {
		send(new RequestBuildDetailedPacket(buildId));
	}
	
	public void loadDeploymentDetailed(long deploymentId) {
		send(new RequestDeploymentDetailedPacket(deploymentId));
	}
	
	private void loadBuildHistory(String serverId) {
		send(new RequestBuildListPacket(serverId));
	}
	
	private void loadDeploymentHistory(String serverId) {
		send(new RequestDeploymentListPacket(serverId));
	}
	
	public void requestCompleteLoad() {
		if (connected.get()) {
			send(new RequestServerListPacket());
		}
	}
	
	public void requestLatestUpdates() {
		SharedServerData server = data.getSelectedServer();
		if (server == null)
			return;
		if (canSendRequestLatestBuild.getAndSet(false)) {
			SharedBuildData lastBuild = server.getLastBuild();
			if (lastBuild != null && lastBuild.getBuildState().isInProgress()) {
				loadBuildDetailed(lastBuild.getId());
			}
		}
		if (canSendRequestLatestDeployment.getAndSet(false)) {
			SharedDeploymentData lastDeployment = server.getLastDeployment();
			if (lastDeployment != null) {
				loadDeploymentDetailed(lastDeployment.getId());
			}
		}
	}
	
	private void handleInboundConnectionUpdateIntent(InboundConnectionUpdateIntent icui) {
		connected.set(icui.isConnected());
		data.clearAll();
		if (icui.isConnected()) {
			for (SharedServerData server : data.getServers()) {
				loadServerDetailed(server.getName());
				loadBuildHistory(server.getName());
				loadDeploymentHistory(server.getName());
			}
			requestCompleteLoad();
		}
	}
	
	private void handleInboundPacketIntent(InboundPacketIntent ipi) {
		Packet packet = ipi.getPacket();
		if (packet instanceof ResponseServerListPacket)
			processServerList((ResponseServerListPacket) packet);
		else if (packet instanceof ResponseServerDetailedPacket)
			processServerDetailed((ResponseServerDetailedPacket) packet);
		else if (packet instanceof ResponseBuildListPacket)
			processBuildList((ResponseBuildListPacket) packet);
		else if (packet instanceof ResponseBuildDetailedPacket)
			processBuildDetailed((ResponseBuildDetailedPacket) packet);
		else if (packet instanceof ResponseDeploymentListPacket)
			processDeploymentList((ResponseDeploymentListPacket) packet);
		else if (packet instanceof ResponseDeploymentDetailedPacket)
			processDeploymentDetailed((ResponseDeploymentDetailedPacket) packet);
		InboundDataUpdateIntent.broadcast(packet);
	}
	
	private void processServerList(ResponseServerListPacket list) {
		List<SharedServerData> current = data.getServers();
		for (SharedServerData server : list.getServerList()) {
			if (!current.contains(server)) {
				data.addServer(server);
				loadServerDetailed(server.getName());
				loadBuildHistory(server.getName());
				loadDeploymentHistory(server.getName());
			}
		}
	}
	
	private void processServerDetailed(ResponseServerDetailedPacket detailed) {
		SharedServerData server = detailed.getServer();
		SharedServerData existing = data.getServer(server.getName());
		if (existing != null) {
			existing.setDirectory(detailed.getDirectory());
			existing.setJvmArguments(detailed.getJvmArguments());
		} else {
			data.addServer(server);
		}
	}
	
	private void processBuildList(ResponseBuildListPacket list) {
		SharedServerData server = data.getServer(list.getServerId());
		SharedBuildData last = server.getLastBuild();
		for (SharedBuildData build : list.getBuildList()) {
			if (server.getBuildData(build.getId()) == null) {
				canSendRequestLatestBuild.set(true);
				server.addBuild(build);
			}
		}
		SharedBuildData nLast = server.getLastBuild();
		if (last != nLast && nLast != null)
			loadBuildDetailed(nLast.getId());
	}
	
	private void processBuildDetailed(ResponseBuildDetailedPacket detailed) {
		canSendRequestLatestBuild.set(true);
		SharedServerData server = data.getServer(detailed.getServerId());
		SharedBuildData build = server.getBuildData(detailed.getBuildId());
		if (build != null) {
			build.setTime(detailed.getTime());
			build.setBuildString(detailed.getBuildString());
			build.setTestString(detailed.getTestString());
			build.setCompileTime(detailed.getCompileTime());
			build.setBuildState(detailed.getBuildState());
			build.setTestDetails(new TestDetails(detailed.getTestTotal(), detailed.getTestFailures()));
		} else {
			server.addBuild(detailed.getBuildData());
		}
	}
	
	private void processDeploymentList(ResponseDeploymentListPacket list) {
		SharedServerData server = data.getServer(list.getServerId());
		SharedDeploymentData last = server.getLastDeployment();
		for (SharedDeploymentData deployment : list.getDeploymentList()) {
			if (server.getDeploymentData(deployment.getId()) == null) {
				canSendRequestLatestDeployment.set(true);
				server.addDeployment(deployment);
			}
		}
		SharedDeploymentData nLast = server.getLastDeployment();
		if (last != nLast && nLast != null)
			loadDeploymentDetailed(nLast.getId());
	}
	
	private void processDeploymentDetailed(ResponseDeploymentDetailedPacket detailed) {
		canSendRequestLatestDeployment.set(true);
		SharedServerData server = data.getServer(detailed.getServerId());
		SharedDeploymentData deployment = server.getDeploymentData(detailed.getDeploymentId());
		if (deployment != null) {
			deployment.setStartTime(detailed.getStartTime());
			deployment.setEndTime(detailed.getEndTime());
			deployment.setOutput(detailed.getOutput());
			deployment.setDeploymentState(detailed.getDeploymentState());
		} else {
			server.addDeployment(detailed.getDeploymentData());
		}
	}
	
}
