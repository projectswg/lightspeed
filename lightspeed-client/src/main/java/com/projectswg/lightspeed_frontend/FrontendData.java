/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend;

import com.projectswg.common.data.SharedBuildData;
import com.projectswg.common.data.SharedDeploymentData;
import com.projectswg.common.data.SharedServerData;
import com.projectswg.common.debug.Log;
import com.projectswg.common.javafx.PSWGBooleanProperty;
import com.projectswg.common.javafx.PSWGObjectProperty;
import com.projectswg.common.javafx.PSWGStringProperty;
import com.projectswg.lightspeed_frontend.communication.HttpClient;
import javafx.application.Platform;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.io.File;
import java.util.prefs.BackingStoreException;
import java.util.prefs.Preferences;

public class FrontendData {
	
	private final ObservableList<SharedServerData> servers;
	private final PSWGObjectProperty<SharedServerData> selectedServer;
	private final SettingsData settingsData;
	
	public FrontendData(Frontend frontend) {
		this.servers = FXCollections.observableArrayList();
		this.selectedServer = new PSWGObjectProperty<>(null);
		this.settingsData = new SettingsData(frontend);
	}
	
	public void close() {
		
	}
	
	public void clearAll() {
		if (!Platform.isFxApplicationThread()) {
			Platform.runLater(this::clearAll);
			return;
		}
		servers.clear();
		selectedServer.set(null);
	}
	
	public void setupSecurity() {
		settingsData.setup();
	}
	
	public SharedServerData getSelectedServer() {
		return selectedServer.get();
	}
	
	public PSWGObjectProperty<SharedServerData> getSelectedServerProperty() {
		return selectedServer;
	}
	
	public ObservableList<SharedServerData> getServers() {
		return servers;
	}
	
	public boolean isSecurity() {
		return settingsData.isSecurity();
	}
	
	public PSWGBooleanProperty getSecurityProperty() {
		return settingsData.getSecurityProperty();
	}
	
	public String getKeystorePath() {
		return settingsData.getKeystorePath();
	}
	
	public PSWGStringProperty getKeystorePathProperty() {
		return settingsData.getKeystorePathProperty();
	}
	
	public String getKeystorePassword() {
		return settingsData.getKeystorePassword();
	}
	
	public PSWGStringProperty getKeystorePasswordProperty() {
		return settingsData.getKeystorePasswordProperty();
	}
	
	public String getUsername() {
		return settingsData.getUsername();
	}
	
	public PSWGStringProperty getUsernameProperty() {
		return settingsData.getUsernameProperty();
	}
	
	public String getPassword() {
		return settingsData.getPassword();
	}
	
	public PSWGStringProperty getPasswordProperty() {
		return settingsData.getPasswordProperty();
	}
	
	public SharedServerData getServer(String serverId) {
		synchronized (servers) {
			for (SharedServerData server : servers) {
				if (server.getName().equals(serverId))
					return server;
			}
		}
		return null;
	}
	
	public SharedBuildData getBuild(long buildId) {
		synchronized (servers) {
			for (SharedServerData server : servers) {
				SharedBuildData build = server.getBuildData(buildId);
				if (build != null)
					return build;
			}
		}
		return null;
	}
	
	public SharedDeploymentData getDeployment(long deploymentId) {
		synchronized (servers) {
			for (SharedServerData server : servers) {
				SharedDeploymentData deployment = server.getDeploymentData(deploymentId);
				if (deployment != null)
					return deployment;
			}
		}
		return null;
	}
	
	public void setSelectedServer(SharedServerData server) {
		this.selectedServer.set(server);
	}
	
	public void addServer(SharedServerData server) {
		synchronized (servers) {
			if (!servers.contains(server))
				servers.add(server);
		}
	}
	
	private static class SettingsData {
		
		private final PSWGBooleanProperty security = new PSWGBooleanProperty();
		private final PSWGStringProperty keystorePath = new PSWGStringProperty();
		private final PSWGStringProperty keystorePassword = new PSWGStringProperty();
		private final PSWGStringProperty username = new PSWGStringProperty();
		private final PSWGStringProperty password = new PSWGStringProperty();
		private final Frontend frontend;
		
		public SettingsData(Frontend frontend) {
			this.frontend = frontend;
		}
		
		public void setup() {
			loadSettings();
			security.addListener(l -> updateKeystore());
			keystorePath.addListener(l -> updateKeystore());
			keystorePassword.addListener(l -> updateKeystore());
			username.addListener(l -> updateKeystore());
			password.addListener(l -> updateKeystore());
		}
		
		public boolean isSecurity() {
			return security.get();
		}
		
		public PSWGBooleanProperty getSecurityProperty() {
			return security;
		}
		
		public String getKeystorePath() {
			return keystorePath.get();
		}
		
		public PSWGStringProperty getKeystorePathProperty() {
			return keystorePath;
		}
		
		public String getKeystorePassword() {
			return keystorePassword.get();
		}
		
		public PSWGStringProperty getKeystorePasswordProperty() {
			return keystorePassword;
		}
		
		public String getUsername() {
			return username.get();
		}
		
		public PSWGStringProperty getUsernameProperty() {
			return username;
		}
		
		public String getPassword() {
			return password.get();
		}
		
		public PSWGStringProperty getPasswordProperty() {
			return password;
		}
		
		private void loadSettings() {
			Preferences settings = getFrontend().getPreferences().node("settings");
			security.set(settings.getBoolean("SECURE", false));
			keystorePath.set(settings.get("KEYSTORE-PATH", ""));
			keystorePassword.set(settings.get("KEYSTORE-PASS", ""));
			username.set(settings.get("USERNAME", ""));
			password.set(settings.get("PASSWORD", ""));
			setupKeystore();
		}
		
		private void setupKeystore() {
			if (!security.get()) {
				HttpClient.setupSSL(null, null);
				requestReload();
				return;
			}
			File keystore = new File(keystorePath.get());
			if (!keystore.isFile()) {
				HttpClient.setupSSL(null, null);
				requestReload();
				Log.w("Invalid keystore file: '%s'", keystore);
				return;
			}
			HttpClient.setupUserPass(username.get(), password.get());
			HttpClient.setupSSL(keystore, keystorePassword.get());
		}
		
		private void updateKeystore() {
			saveSettings();
			setupKeystore();
			HttpClient.restart();
		}
		
		private void saveSettings() {
			Preferences settings = getFrontend().getPreferences().node("settings");
			settings.putBoolean("SECURE", security.get());
			settings.put("KEYSTORE-PATH", keystorePath.get());
			settings.put("KEYSTORE-PASS", keystorePassword.get());
			settings.put("USERNAME", username.get());
			settings.put("PASSWORD", password.get());
			try {
				settings.flush();
			} catch (BackingStoreException e) {
				Log.e(e);
			}
		}
		
		private Frontend getFrontend() {
			return frontend;
		}
		
		private void requestReload() {
			getFrontend().requestCompleteLoad();
		}
		
	}
	
}
