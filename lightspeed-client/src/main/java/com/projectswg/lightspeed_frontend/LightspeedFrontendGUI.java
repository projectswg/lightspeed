/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend;

import com.projectswg.common.control.IntentManager;
import com.projectswg.common.debug.Log;
import com.projectswg.common.debug.Log.LogLevel;
import com.projectswg.common.debug.ThreadPrinter;
import com.projectswg.common.debug.log_wrapper.ConsoleLogWrapper;
import com.projectswg.common.debug.log_wrapper.FileLogWrapper;
import com.projectswg.common.javafx.FXMLUtilities;
import com.projectswg.common.javafx.ResourceUtilities;
import com.projectswg.lightspeed_frontend.gui.LightspeedPrimaryView;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.stage.Stage;

import java.io.File;
import java.util.Locale;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class LightspeedFrontendGUI extends Application {
	
	private static final AtomicBoolean SHUTDOWN_HOOK = new AtomicBoolean(false);
	
	private static final AtomicReference<Frontend> FRONTEND = new AtomicReference<>(new Frontend());
	
	public static void main(String[] args) {
		ThreadGroup group = new ThreadGroup("lightspeed");
		Thread mainThread = new Thread(group, LightspeedFrontendGUI::mainThread, "main");
		Runtime.getRuntime().addShutdownHook(createShutdownHook(mainThread));
		
		mainThread.start();
		try {
			mainThread.join();
		} catch (InterruptedException e) {
			// Expected
		}
		
		if (!SHUTDOWN_HOOK.get())
			System.exit(0);
	}
	
	private static void mainThread() {
		try {
			ResourceUtilities.setPrimarySource(LightspeedFrontendGUI.class);
			Log.addWrapper(new ConsoleLogWrapper(LogLevel.VERBOSE));
			Log.addWrapper(new FileLogWrapper(new File("frontend_log.txt")));
			
			IntentManager intentManager = new IntentManager(Runtime.getRuntime().availableProcessors());
			intentManager.initialize();
			IntentManager.setInstance(intentManager);
			
			try {
				launch();
			} catch (Throwable t) {
				if (!(t.getCause() instanceof InterruptedException))
					Log.e(t);
			}
			
			intentManager.terminate();
			ThreadPrinter.printActiveThreads();
		} catch (Throwable t) {
			Log.e(t);
		}
	}
	
	private static Thread createShutdownHook(Thread mainThread) {
		Thread currentThread = Thread.currentThread();
		Thread thread = new Thread(() -> {
			SHUTDOWN_HOOK.set(true);
			currentThread.interrupt();
			mainThread.interrupt();
			try {
				mainThread.join();
			} catch (InterruptedException e) {
				Log.e(e);
			}
		}, "lightspeed-shutdown-hook");
		thread.setDaemon(true);
		return thread;
	}
	
	@Override
	public void start(Stage primaryStage) {
		getFrontend().setPrimaryStage(primaryStage);
		LightspeedPrimaryView primaryView = (LightspeedPrimaryView) FXMLUtilities.loadFxmlAsClassResource("res/fxml/PrimaryView.fxml", Locale.ENGLISH);
		if (primaryView == null) {
			Log.e("Failed to load primary view fxml");
			FXMLUtilities.showErrorMessage("Failed to load fxml", "Failed to load PrimaryView.fxml - could be caused by an invalid jar");
			Platform.runLater(Platform::exit);
			return;
		}
		Parent root = primaryView.getRoot();
		primaryStage.setTitle("Lightspeed Frontend");
		primaryStage.setScene(new Scene(root));
		root.setOnMouseClicked((event) -> root.requestFocus());
		root.requestFocus();
		primaryStage.show();
		primaryStage.setOnCloseRequest((we) -> Platform.exit());
		getFrontend().start();
	}
	
	@Override
	public void stop() {
		FXMLUtilities.terminate();
		getFrontend().stop();
	}
	
	public static Frontend getFrontend() {
		return FRONTEND.get();
	}
	
}
