/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.communication;

import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.common.network.packets.LightspeedPacketType;
import com.projectswg.lightspeed_frontend.intents.InboundConnectionUpdateIntent;
import com.projectswg.lightspeed_frontend.intents.InboundPacketIntent;
import me.joshlarson.json.JSONObject;
import me.joshlarson.json.websocket.client.CloseCode;
import me.joshlarson.json.websocket.client.JSONWebSocketClient;
import me.joshlarson.json.websocket.client.JSONWebSocketHandler;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.KeyManagerFactory;
import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.InetAddress;
import java.net.URI;
import java.net.URISyntaxException;
import java.security.KeyStore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicReference;

public class HttpClient {
	
	private static final AtomicBoolean SSL = new AtomicBoolean(false);
	private static final AtomicReference<String> USERNAME = new AtomicReference<>("");
	private static final AtomicReference<String> PASSWORD = new AtomicReference<>("");
	private static final AtomicReference<URI> ADDRESS = new AtomicReference<>(null);
	private static final JSONWebSocketClient CLIENT = new JSONWebSocketClient();
	
	public static void setupSSL(File keystoreFile, String password) {
		if (keystoreFile == null || password == null) {
			SSL.set(false);
			reconfigureAddress();
			return;
		}
		try {
			InputStream keystoreStream = new FileInputStream(keystoreFile);
			char[] trustPassword = password.toCharArray();
			KeyStore keystore = KeyStore.getInstance(KeyStore.getDefaultType());

			keystore.load(keystoreStream, trustPassword);
			KeyManagerFactory keyManagerFactory = KeyManagerFactory.getInstance(KeyManagerFactory.getDefaultAlgorithm());
			keyManagerFactory.init(keystore, trustPassword);
			TrustManagerFactory trustManagerFactory = TrustManagerFactory.getInstance(TrustManagerFactory.getDefaultAlgorithm());
			trustManagerFactory.init(keystore);
			SSLContext ctx = SSLContext.getInstance("TLS");
			ctx.init(keyManagerFactory.getKeyManagers(), trustManagerFactory.getTrustManagers(), null);
			SSLContext.setDefault(ctx);
			HttpsURLConnection.setDefaultHostnameVerifier((hostname, session) -> true);
			SSL.set(true);
			reconfigureAddress();
		} catch (Exception e) {
			Log.e(e);
			SSL.set(false);
			reconfigureAddress();
		}
	}
	
	private static void reconfigureAddress() {
		try {
			URI uri = ADDRESS.get();
			if (uri != null)
				ADDRESS.compareAndSet(uri, new URI(SSL.get() ? "wss" : "ws", uri.getUserInfo(), uri.getHost(), uri.getPort(), uri.getPath(), uri.getQuery(), uri.getFragment()));
		} catch (URISyntaxException e) {
			Log.e(e);
		}
	}
	
	public static void setupUserPass(String user, String pass) {
		USERNAME.set(user);
		PASSWORD.set(pass);
	}
	
	public static void setupAddress(InetAddress address, int port) {
		URI uri;
		try {
			uri = new URI(SSL.get() ? "wss" : "ws", null, address.getHostName(), port, null, null, null);
		} catch (URISyntaxException e) {
			Log.e(e);
			return;
		}
		URI prev = ADDRESS.getAndSet(uri);
		if (prev != null) {
			if (prev.equals(uri))
				return;
			terminate();
		}
	}
	
	public static boolean isSSLEnabled() {
		return SSL.get();
	}
	
	public static void restart() {
		Log.d("Restarting HttpClient");
		terminate();
		initialize();
	}
	
	public static void initialize() {
		CLIENT.setVerifyHostname(false);
		CLIENT.setHandler(new JSONWebSocketHandler() {
			
			public void onConnect(JSONWebSocketClient socket) { HttpClient.onConnected(); }
			
			public void onDisconnect(JSONWebSocketClient socket) { HttpClient.onDisconnected(); }
			
			public void onMessage(JSONWebSocketClient socket, JSONObject object) { HttpClient.onMessage(object); }
			
			public void onPongTimed(JSONWebSocketClient socket, long rttNano) { HttpClient.onPongTimed(rttNano); }
			
			public void onError(JSONWebSocketClient socket, Throwable t) { HttpClient.onError(t); }
		});
		tryConnect();
	}
	
	public static void terminate() {
		if (CLIENT.isConnected())
			CLIENT.disconnect(CloseCode.GOING_AWAY, "");
	}
	
	public static boolean probeConnection() {
		if (ADDRESS.get() == null || !tryConnect())
			return false;
		CLIENT.pingTimed();
		return CLIENT.isConnected();
	}
	
	public static void requestPacket(Packet packet) {
		if (!tryConnect()) {
			Log.e("Failed to connect to %s", ADDRESS.get());
			return;
		}
		CLIENT.send(packet.getJSON());
	}
	
	private static void onConnected() {
		Log.d("HttpClient: onConnected()");
		InboundConnectionUpdateIntent.broadcast(true);
	}
	
	private static void onDisconnected() {
		Log.d("HttpClient: onDisconnected()");
		InboundConnectionUpdateIntent.broadcast(false);
	}
	
	private static void onMessage(JSONObject object) {
		if (!object.containsKey("type")) {
			Log.d("RX %s", object.toString(true));
			return;
		}
		LightspeedPacketType request = LightspeedPacketType.valueOf(object.getString("type"));
		Packet packet = Packet.inflate(object, request);
		if (packet == null) {
			Log.e("HttpClient: Failed to inflate %s [%s]", request, object.getString("type"));
			return;
		}
		
		InboundPacketIntent.broadcast(packet);
	}
	
	private static void onPongTimed(long rttNano) {
		Log.d("Round trip time: %.3fms", rttNano / 1E6);
	}
	
	private static void onError(Throwable t) {
		Log.e(t);
	}
	
	private static boolean tryConnect() {
		synchronized (CLIENT) {
			if (CLIENT.isConnected())
				return true;
			if (ADDRESS.get() == null)
				return false;
			try {
				Log.i("Attempting connection to %s", ADDRESS.get());
				CLIENT.setUserInfo(USERNAME.get(), PASSWORD.get());
				CLIENT.connect(ADDRESS.get());
				return true;
			} catch (IOException e) {
				Log.e("Connection Error: %s", e.getMessage());
				return false;
			}
		}
	}
}
