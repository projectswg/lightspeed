/***********************************************************************************
 * Copyright (c) 2018 /// Project SWG /// www.projectswg.com                       *
 *                                                                                 *
 * ProjectSWG is the first NGE emulator for Star Wars Galaxies founded on          *
 * July 7th, 2011 after SOE announced the official shutdown of Star Wars Galaxies. *
 * Our goal is to create an emulator which will provide a server for players to    *
 * continue playing a game similar to the one they used to play. We are basing     *
 * it on the final publish of the game prior to end-game events.                   *
 *                                                                                 *
 * This file is part of Lightspeed.                                                *
 *                                                                                 *
 * --------------------------------------------------------------------------------*
 *                                                                                 *
 * Lightspeed is free software: you can redistribute it and/or modify              *
 * it under the terms of the GNU Affero General Public License as                  *
 * published by the Free Software Foundation, either version 3 of the              *
 * License, or (at your option) any later version.                                 *
 *                                                                                 *
 * Lightspeed is distributed in the hope that it will be useful,                   *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of                  *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the                   *
 * GNU Affero General Public License for more details.                             *
 *                                                                                 *
 * You should have received a copy of the GNU Affero General Public License        *
 * along with Lightspeed.  If not, see <http://www.gnu.org/licenses/>.             *
 *                                                                                 *
 ***********************************************************************************/
package com.projectswg.lightspeed_frontend.communication;

import com.projectswg.common.debug.Assert;
import com.projectswg.common.debug.Log;
import com.projectswg.common.network.packets.Packet;
import com.projectswg.lightspeed_frontend.Frontend;

import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.UnknownHostException;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.prefs.Preferences;

public class FrontendCommunication {
	
	private final Frontend frontend;
	private final AtomicBoolean running;
	private InetSocketAddress address;
	
	public FrontendCommunication(Frontend frontend) {
		this.frontend = frontend;
		this.running = new AtomicBoolean(false);
		try {
			Preferences comm = frontend.getPreferences().node("communication");
			InetAddress address = InetAddress.getByName(comm.get("inet-addr", "localhost"));
			int port = comm.getInt("inet-port", 44444);
			this.address = new InetSocketAddress(address, port);
		} catch (UnknownHostException e) {
			Log.e(e);
			this.address = new InetSocketAddress(InetAddress.getLoopbackAddress(), 44444);
		}
	}
	
	public InetSocketAddress getAddress() {
		return address;
	}
	
	public void setAddress(InetSocketAddress address) {
		Preferences comm = frontend.getPreferences().node("communication");
		if (address == null) {
			comm.remove("inet-addr");
			comm.remove("inet-port");
		} else if (!address.equals(this.address)) {
			comm.put("inet-addr", address.getHostString());
			comm.putInt("inet-port", address.getPort());
			HttpClient.setupAddress(address.getAddress(), address.getPort());
			if (running.get())
				HttpClient.restart();
		}
		this.address = address;
	}
	
	public boolean start() {
		if (running.getAndSet(true)) {
			Assert.fail();
			return false;
		}
		InetSocketAddress address = this.address;
		if (address == null)
			address = new InetSocketAddress(InetAddress.getLoopbackAddress(), 44444);
		HttpClient.setupAddress(address.getAddress(), address.getPort());
		HttpClient.initialize();
		return true;
	}
	
	public boolean stop() {
		if (!running.getAndSet(false)) {
			Assert.fail();
			return false;
		}
		return true;
	}
	
	public boolean probeConnectivity() {
		return HttpClient.probeConnection();
	}
	
	public void send(Packet packet) {
		HttpClient.requestPacket(packet);
	}
	
}
