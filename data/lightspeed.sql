CREATE TABLE IF NOT EXISTS builds (
	id INTEGER PRIMARY KEY,
	time INTEGER,
	server_id TEXT,
	build_string TEXT,
	test_string TEXT,
	compile_time REAL,
	offline INTEGER,
	build_state TEXT,
	test_total INTEGER,
	test_failures INTEGER
);

CREATE TABLE IF NOT EXISTS deployments (
	id INTEGER PRIMARY KEY,
	server_id TEXT,
	build_id INTEGER REFERENCES builds(id),
	start_time INTEGER,
	end_time INTEGER
);
